use ashpd::desktop::open_uri::OpenFileRequest;
use futures_util::Future;
use gio::{ListStore, NetworkConnectivity, SimpleAction};
use glib::Priority;
use libadwaita::traits::NavigationPageExt;
use reqwest::Client;
use std::cell::RefCell;
use std::collections::HashSet;
use std::fmt::Debug;
use std::path::PathBuf;
use std::rc::Rc;
use std::sync::Arc;
use std::time;

use crate::add_dialog::{AddCategoryDialog, AddFeedDialog, AddTagDialog};
use crate::content_page::{ArticleListMode, ContentPageState};
use crate::edit_category_dialog::EditCategoryDialog;
use crate::edit_feed_dialog::EditFeedDialog;
use crate::error::NewsFlashGtkError;
use crate::error_dialog::ErrorDialog;
use crate::i18n::{i18n, i18n_f};
use futures::channel::oneshot::{self, Sender as OneShotSender};
use futures::FutureExt;
use gdk4::prelude::*;
use gio::{
    prelude::PowerProfileMonitorExt, subclass::prelude::*, ApplicationFlags, Notification, NotificationPriority,
    ThemedIcon,
};
use glib::{clone, subclass::types::ObjectSubclass, ControlFlow, SourceId};
use gtk4::{prelude::*, FileDialog};
use gtk4::{subclass::prelude::GtkApplicationImpl, FileFilter};
use libadwaita::subclass::prelude::*;
use log::{error, info, warn};
use news_flash::models::{
    ArticleID, CategoryID, CategoryMapping, FavIcon, Feed, FeedID, FeedMapping, LoginData, PluginCapabilities, Tag,
    TagID, Thumbnail, Url,
};
use news_flash::{error::NewsFlashError, NewsFlash};
use once_cell::sync::Lazy;
use tokio::runtime::Runtime;
use tokio::sync::RwLock;
use tokio::time::{timeout, Duration};

use crate::about_dialog::NewsFlashAbout;
use crate::article_list::{MarkUpdate, ReadUpdate};
use crate::article_view::ArticleView;
use crate::config::{APP_ID, VERSION};
use crate::discover::DiscoverDialog;
use crate::edit_tag_dialog::EditTagDialog;
use crate::main_window::MainWindow;
use crate::settings::{GFeedOrder, Settings, SettingsDialog};
use crate::shortcuts_dialog::ShortcutsDialog;
use crate::undo_action::UndoDelete;
use crate::util::{constants, DesktopSettings, GtkUtil, Util, CHANNEL_ERROR};

pub static CONFIG_DIR: Lazy<PathBuf> = Lazy::new(|| glib::user_config_dir().join("news-flash"));
pub static DATA_DIR: Lazy<PathBuf> = Lazy::new(|| glib::user_data_dir().join("news-flash"));
pub static WEBKIT_DATA_DIR: Lazy<PathBuf> = Lazy::new(|| DATA_DIR.join("Webkit"));
pub static IMAGE_DATA_DIR: Lazy<PathBuf> = Lazy::new(|| DATA_DIR.join("pictures"));

#[derive(Debug, Clone)]
pub struct NotificationCounts {
    pub new: i64,
    pub unread: i64,
}

pub struct AppPrivate {
    pub window: RefCell<Option<MainWindow>>,
    pub news_flash: Arc<RwLock<Option<NewsFlash>>>,
    pub news_flash_error: RefCell<Option<NewsFlashGtkError>>,
    pub settings: Rc<RefCell<Settings>>,
    pub sync_source_id: RefCell<Option<SourceId>>,
    pub runtime: Runtime,
    pub client: RefCell<Client>,
    pub shutdown_in_progress: Rc<RefCell<bool>>,
    pub start_headless: RefCell<bool>,
    pub features: RefCell<PluginCapabilities>,
    pub desktop_settings: DesktopSettings,
}

#[glib::object_subclass]
impl ObjectSubclass for AppPrivate {
    const NAME: &'static str = "AppPrivate";
    type Type = App;
    type ParentType = libadwaita::Application;

    fn new() -> Self {
        info!("NewsFlash {} ({})", VERSION, APP_ID);

        let shutdown_in_progress = Rc::new(RefCell::new(false));
        let start_headless = RefCell::new(false);

        let runtime = Runtime::new().expect("Error creating tokio runtime");
        let news_flash_lib = NewsFlash::try_load(&DATA_DIR, &CONFIG_DIR).ok();

        if news_flash_lib.is_some() {
            info!("Successful load from config");
        } else {
            warn!("No account configured");
        }

        let news_flash = Arc::new(RwLock::new(news_flash_lib));
        let features = RefCell::new(PluginCapabilities::NONE);
        let settings = Settings::open().expect("Failed to access settings file");
        let settings = Rc::new(RefCell::new(settings));
        let desktop_settings = DesktopSettings::default();

        let client = RefCell::new(Util::build_client(settings.clone()));

        Self {
            window: RefCell::new(None),
            news_flash,
            news_flash_error: RefCell::new(None),
            settings,
            sync_source_id: RefCell::new(None),
            runtime,
            client,
            shutdown_in_progress,
            start_headless,
            features,
            desktop_settings,
        }
    }
}

impl ObjectImpl for AppPrivate {}

impl GtkApplicationImpl for AppPrivate {}

impl ApplicationImpl for AppPrivate {
    fn startup(&self) {
        // Workaround to still load style.css if app-id has '.Devel' suffix
        let devel_suffix = ".Devel";
        if APP_ID.ends_with(devel_suffix) {
            if let Some(id_str) = APP_ID.strip_suffix(devel_suffix) {
                let new_id = format!("/{}/", id_str.replace('.', "/"));
                self.obj().set_resource_base_path(Some(&new_id));
            }
        }

        GtkUtil::register_symbolic_icons();
        GtkUtil::register_styles();

        // callback will only be executed if news_flash is initialized
        self.obj().execute_with_callback(
            |_, _| async {},
            |app, ()| {
                app.schedule_sync();
            },
        );

        self.parent_startup();
    }

    fn activate(&self) {
        if let Some(window) = self.window.borrow().as_ref() {
            window.set_visible(true);
            window.present();

            return;
        }

        self.desktop_settings.init();

        let window = MainWindow::new();
        self.window.borrow_mut().replace(window);
        let obj = self.obj();
        let main_window = obj.main_window();
        obj.add_window(&main_window);
        main_window.init(self.shutdown_in_progress.clone());

        obj.setup_actions();
        obj.update_features();
        obj.restore_state();

        // update client if network changes
        let network_monitor = gio::NetworkMonitor::default();
        if !network_monitor.is_network_available() && network_monitor.connectivity() != NetworkConnectivity::Full {
            App::default().set_offline(true);
        }

        let settings = self.settings.clone();
        let client = self.client.clone();
        network_monitor.connect_network_changed(move |monitor, network_available| {
            let connectivity = monitor.connectivity();
            log::info!("Network Changed! connectivity: {connectivity} (available {network_available})");

            let is_offline_mode = App::default()
                .main_window()
                .content_page()
                .state()
                .borrow()
                .get_offline();

            if network_available && connectivity == NetworkConnectivity::Full {
                if is_offline_mode {
                    App::default().set_offline(false);
                }
                client.replace(Util::build_client(settings.clone()));
            } else if !is_offline_mode {
                App::default().set_offline(true);
            }
        });

        if *self.start_headless.borrow() {
            main_window.set_visible(false);
        } else {
            main_window.present();
        }

        if self.settings.borrow().get_sync_on_startup() {
            info!("Startup Sync");
            self.obj().sync();
        }
    }
}

impl AdwApplicationImpl for AppPrivate {}

glib::wrapper! {
    pub struct App(ObjectSubclass<AppPrivate>)
        @extends gio::Application, gtk4::Application, libadwaita::Application;
}

impl App {
    pub fn run(allow_webview_inspector: bool, start_headless: bool) {
        let app: App = glib::Object::builder()
            .property("application-id", Some(APP_ID))
            .property("flags", ApplicationFlags::empty())
            .build();

        let private = app.imp();
        private
            .settings
            .borrow_mut()
            .set_inspect_article_view(allow_webview_inspector);
        private.start_headless.replace(start_headless);
        let empty_args: Vec<&str> = vec![];
        ApplicationExtManual::run_with_args(&app, &empty_args);
    }

    pub fn default() -> App {
        gio::Application::default()
            .expect("Failed to get default gio::Application")
            .downcast::<App>()
            .expect("failed to downcast gio::Application to App")
    }

    pub fn execute<Fut, FTokio>(&self, async_fn: FTokio)
    where
        Fut: Future<Output = ()> + Send + 'static,
        FTokio: FnOnce(Arc<RwLock<Option<NewsFlash>>>, Client) -> Fut + Send + 'static,
    {
        let imp = self.imp();
        let news_flash = imp.news_flash.clone();
        let client = imp.client.borrow().clone();
        let thread_future = async move { async_fn(news_flash, client).await };

        self.imp().runtime.spawn(thread_future);
    }

    pub fn execute_with_callback<T, Fut, FTokio, FGLib>(&self, async_fn: FTokio, callback: FGLib)
    where
        T: Send + 'static + Debug,
        Fut: Future<Output = T> + Send + 'static,
        FTokio: FnOnce(Arc<RwLock<Option<NewsFlash>>>, Client) -> Fut + Send + 'static,
        FGLib: FnOnce(App, T) + 'static,
    {
        let (sender, receiver) = oneshot::channel::<T>();

        let imp = self.imp();
        let news_flash = imp.news_flash.clone();
        let client = imp.client.borrow().clone();
        let f = async move {
            let res = async_fn(news_flash, client).await;
            sender.send(res).expect(CHANNEL_ERROR);
        };

        let glib_future = receiver.map(clone!(
            @weak self as this => @default-panic, move |res|
        {
            if let Ok(res) = res {
                callback(this, res);
            }
        }));

        self.imp().runtime.spawn(f);
        glib::MainContext::default().spawn_local(glib_future);
    }

    fn update_features(&self) {
        self.execute_with_callback(
            |news_flash, _client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    news_flash.features().await.ok()
                } else {
                    None
                }
            },
            |app, res| {
                let mut support_mutation = false;
                let mut tags = false;

                if let Some(features) = res {
                    support_mutation = features.support_mutation();
                    tags = features.contains(PluginCapabilities::SUPPORT_TAGS);
                    app.imp().features.replace(features);
                }

                let main_window = app.main_window();
                main_window.update_features();

                app.set_action_enabled("enqueue-delete-category", support_mutation);
                app.set_action_enabled("edit-category-dialog", support_mutation);
                app.set_action_enabled("enqueue-delete-feed", support_mutation);
                app.set_action_enabled("enqueue-delete-tag", support_mutation);
                app.set_action_enabled("edit-tag-dialog", support_mutation);
                app.set_action_enabled("import-opml", support_mutation);
                app.set_action_enabled("discover", support_mutation);

                app.set_action_enabled("add-feed", support_mutation);
                app.set_action_enabled("add-category", support_mutation);
                app.set_action_enabled("add-tag", tags);
            },
        );
    }

    fn restore_state(&self) {
        glib::idle_add_local(clone!(@weak self as app => @default-panic, move || {
            let state = app.content_page_state();
            let visible_article = state.borrow().get_visible_article_id();
            let sidebar_selection = state.borrow().get_sidebar_selection().clone();
            let article_list_mode = state.borrow().get_article_list_mode().clone();
            let search_term = state.borrow().get_search_term().map(String::from);
            let maximized = state.borrow().get_maximized();

            let main_window = app.main_window();
            let content_page = main_window.content_page();
            let sidebar = content_page.sidebar_column().sidebar();
            let article_list_column = content_page.article_list_column();
            let article_list = content_page.article_list_column().article_list();

            main_window.set_maximized(maximized);
            sidebar.restore_selection(sidebar_selection);

            match article_list_mode {
                ArticleListMode::All => article_list_column.set_view_switcher_stack("all"),
                ArticleListMode::Unread => article_list_column.set_view_switcher_stack("unread"),
                ArticleListMode::Marked => article_list_column.set_view_switcher_stack("marked"),
            };

            if let Some(visible_article) = visible_article {
                article_list.select_article(&visible_article);
            }

            if let Some(search_term) = search_term {
                article_list_column.set_search_term(&search_term);
            }

            ControlFlow::Break
        }));
    }

    pub fn features(&self) -> PluginCapabilities {
        *self.imp().features.borrow()
    }

    pub fn settings(&self) -> Rc<RefCell<Settings>> {
        self.imp().settings.clone()
    }

    pub fn content_page_state(&self) -> Rc<RefCell<ContentPageState>> {
        self.main_window().content_page().state()
    }

    pub fn current_undo_action(&self) -> Option<UndoDelete> {
        self.main_window().content_page().get_current_undo_action()
    }

    pub fn processing_undo_actions(&self) -> Rc<RefCell<HashSet<UndoDelete>>> {
        self.main_window().content_page().processing_undo_actions()
    }

    pub fn desktop_settings(&self) -> &DesktopSettings {
        &self.imp().desktop_settings
    }

    pub fn set_newsflash_error(&self, error: NewsFlashGtkError) {
        self.imp().news_flash_error.replace(Some(error));
    }

    fn spawn_about_window(&self) {
        NewsFlashAbout::show(self, &self.main_window());
    }

    fn set_action_enabled(&self, action_name: &str, enabled: bool) {
        if let Some(action) = self.main_window().lookup_action(action_name) {
            if let Ok(simple_action) = action.downcast::<SimpleAction>() {
                simple_action.set_enabled(enabled);
            }
        }
    }

    fn setup_actions(&self) {
        let main_window = self.main_window();

        // -------------------------
        // edit category dialog
        // -------------------------
        let type_ = glib::VariantType::new("s").unwrap();
        let edit_category_dialog_action = SimpleAction::new("edit-category-dialog", Some(&type_));
        edit_category_dialog_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, parameter| {
                if let Some(category_id_str) = parameter.and_then(|p| p.str()) {
                    let category_id = CategoryID::new(category_id_str);

                    this.execute_with_callback(
                        |news_flash, _client| async move {
                            if let Some(news_flash) = news_flash.read().await.as_ref() {
                                let categories = news_flash.get_categories().map(|(c, _m)| c).ok()?;
                                let category = categories.into_iter().find(|c| c.category_id == category_id)?;
                                Some(category)
                            } else {
                                None
                            }
                        },
                        |app, res| {
                            if let Some(category) = res {
                                EditCategoryDialog::new(&app.main_window(), category);
                            } else {
                                error!("Failed to find category for edit dialog");
                            }
                        }
                    );
                } else {
                    App::default().in_app_notifiaction(&i18n("Edit category: no parameter"));
                }
            }),
        );

        // -------------------------
        // delete category
        // -------------------------
        let type_ = glib::VariantType::new("(ss)").unwrap();
        let delete_category_action = SimpleAction::new("enqueue-delete-category", Some(&type_));
        delete_category_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, parameter| {
                if let Some(parameter) = parameter {
                    let id_value = parameter.child_value(0);
                    let name_value = parameter.child_value(1);

                    if let (Some(id_str), Some(name_str)) = (id_value.str(), name_value.str()) {
                        let category_id = CategoryID::new(id_str);
                        this.main_window().show_undo_bar(UndoDelete::Category(category_id, name_str.into()));
                    }
                } else {
                    App::default().in_app_notifiaction(&i18n("Delete category: no parameter"));
                }
            }),
        );

        // -------------------------
        // edit feed dialog
        // -------------------------
        let type_ = glib::VariantType::new("(ss)").unwrap();
        let edit_feed_dialog_action = SimpleAction::new("edit-feed-dialog", Some(&type_));
        edit_feed_dialog_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, parameter| {
                if let Some(parameter) = parameter {
                    let id_value = parameter.child_value(0);
                    let parent_value = parameter.child_value(1);

                    if let (Some(id_str), Some(parent_str)) = (id_value.str(), parent_value.str()) {
                        let feed_id = FeedID::new(id_str);
                        let parent_id = CategoryID::new(parent_str);
                        let feed_mapping = FeedMapping {
                            feed_id: feed_id.clone(),
                            category_id: parent_id,
                            sort_index: None
                        };

                        this.execute_with_callback(
                            |news_flash, _client| async move {
                                if let Some(news_flash) = news_flash.read().await.as_ref() {
                                    let categories = news_flash.get_categories().map(|(c, _m)| c).ok()?;
                                    let feeds = news_flash.get_feeds().map(|(f, _m)| f).ok()?;
                                    let feed = feeds.into_iter().find(|f| f.feed_id == feed_id)?;
                                    Some((feed, categories))
                                } else {
                                    None
                                }
                            },
                            move |app, res| {
                                if let Some((feed, categories)) = res {
                                    EditFeedDialog::new(&app.main_window(), feed, feed_mapping, categories);
                                } else {
                                    error!("Failed to find feed for edit dialog");
                                }
                            }
                        );
                    }
                } else {
                    App::default().in_app_notifiaction(&i18n("Edit feed: no parameter"));
                }
            }),
        );

        // -------------------------
        // delete feed
        // -------------------------
        let type_ = glib::VariantType::new("(ss)").unwrap();
        let delete_feed_action = SimpleAction::new("enqueue-delete-feed", Some(&type_));
        delete_feed_action.connect_activate(clone!(@weak self as this => @default-panic, move |_action, parameter| {
            if let Some(parameter) = parameter {
                let id_value = parameter.child_value(0);
                let name_value = parameter.child_value(1);

                if let (Some(id_str), Some(name_str)) = (id_value.str(), name_value.str()) {
                    let feed_id = FeedID::new(id_str);
                    this.main_window().show_undo_bar(UndoDelete::Feed(feed_id, name_str.into()));
                }
            } else {
                App::default().in_app_notifiaction(&i18n("Delete feed: no parameter"));
            }
        }));

        // -------------------------
        // edit tag dialog
        // -------------------------
        let type_ = glib::VariantType::new("s").unwrap();
        let edit_tag_dialog_action = SimpleAction::new("edit-tag-dialog", Some(&type_));
        edit_tag_dialog_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, parameter| {
                if let Some(id_str) = parameter.and_then(|p| p.str()) {
                    let tag_id = TagID::new(id_str);

                    this.execute_with_callback(
                        |news_flash, _client| async move {
                            if let Some(news_flash) = news_flash.read().await.as_ref() {
                                let tags = news_flash.get_tags().map(|(tags, _taggings)| tags).ok()?;
                                let tag = tags.into_iter().find(|t| t.tag_id == tag_id)?;
                                Some(tag)
                            } else {
                                None
                            }
                        },
                        |app, res| {
                            if let Some(tag) = res {
                                EditTagDialog::new(&app.main_window(), tag);
                            } else {
                                error!("Failed to find tag for edit dialog");
                            }
                        }
                    );
                }
            }),
        );

        // -------------------------
        // delete tag
        // -------------------------
        let type_ = glib::VariantType::new("(ss)").unwrap();
        let delete_tag_action = SimpleAction::new("enqueue-delete-tag", Some(&type_));
        delete_tag_action.connect_activate(clone!(@weak self as this => @default-panic, move |_action, parameter| {
            if let Some(parameter) = parameter {
                let id_value = parameter.child_value(0);
                let name_value = parameter.child_value(1);

                if let (Some(id_str), Some(name_str)) = (id_value.str(), name_value.str()) {
                    let tag_id = TagID::new(id_str);
                    this.main_window().show_undo_bar(UndoDelete::Tag(tag_id, name_str.into()));
                }
            } else {
                App::default().in_app_notifiaction(&i18n("Delete Tag: no parameter"));
            }
        }));

        // -------------------------
        // show shortcuts dialog
        // -------------------------
        let show_shortcut_window_action = SimpleAction::new("shortcut-window", None);
        show_shortcut_window_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, _parameter| {
                this.spawn_shortcut_window();
            }),
        );

        // -------------------------
        // about dialog
        // -------------------------
        let show_about_window_action = SimpleAction::new("about-window", None);
        show_about_window_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, _parameter| {
                this.spawn_about_window();
            }),
        );

        // -------------------------
        // show settings dialog
        // -------------------------
        let settings_window_action = SimpleAction::new("settings", None);
        settings_window_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, _parameter| {
                this.spawn_settings_window();
            }),
        );

        // -------------------------
        // show discover dialog
        // -------------------------
        let discover_dialog_action = SimpleAction::new("discover", None);
        discover_dialog_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, _parameter| {
                this.spawn_discover_dialog();
            }),
        );

        // -------------------------
        // add tag dialog
        // -------------------------
        let add_tag_action = SimpleAction::new("add-tag", None);
        add_tag_action.connect_activate(|_action, _parameter| AddTagDialog::new().present());

        // -------------------------
        // add category dialog
        // -------------------------
        let add_category_action = SimpleAction::new("add-category", None);
        add_category_action.connect_activate(|_action, _parameter| AddCategoryDialog::new().present());

        // -------------------------
        // add feed dialog
        // -------------------------
        let add_feed_action = SimpleAction::new("add-feed", None);
        add_feed_action
            .connect_activate(|_action, _parameter| AddFeedDialog::new(&App::default().main_window()).present());

        // -------------------------
        // quit app
        // -------------------------
        let quit_action = SimpleAction::new("quit-application", None);
        quit_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, _parameter| {
                this.queue_quit();
            }),
        );

        // -------------------------
        // import opml
        // -------------------------
        let import_opml_action = SimpleAction::new("import-opml", None);
        import_opml_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, _parameter| {
                this.import_opml();
            }),
        );

        // -------------------------
        // export opml
        // -------------------------
        let export_opml_action = SimpleAction::new("export-opml", None);
        export_opml_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, _parameter| {
                this.export_opml();
            }),
        );

        // -------------------------
        // save image
        // -------------------------
        let type_ = glib::VariantType::new("s").unwrap();
        let save_image_action = SimpleAction::new("save-webview-image", Some(&type_));
        save_image_action.connect_activate(clone!(@weak self as this => @default-panic, move |_action, parameter| {
            if let Some(image_uri) = parameter.and_then(|p| p.str()) {
                this.save_image(image_uri);
            } else {
                App::default().in_app_notifiaction(&i18n("save webview image: no parameter"));
            }
        }));

        // -------------------------
        // reset account
        // -------------------------
        let reset_account_action = SimpleAction::new("reset-account", None);
        reset_account_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, _parameter| {
                this.main_window().show_reset_page()
            }),
        );

        // -------------------------
        // update account login
        // -------------------------
        let update_login_action = SimpleAction::new("update-login", None);
        update_login_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, _parameter| {
                this.update_login();
            }),
        );

        // -------------------------
        // close article
        // -------------------------
        let close_article_action = SimpleAction::new("close-article", None);
        close_article_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, _parameter| {
                let main_window = this.main_window();
                let article_view_column = main_window.content_page().articleview_column();

                // clear view
                article_view_column.article_view().close_article();
                // update headerbar
                article_view_column.show_article(None, None);
            }),
        );
        close_article_action.set_enabled(false);

        // -------------------------
        // fullscreen article
        // -------------------------
        let fullscreen_article_action = SimpleAction::new("fullscreen-article", None);
        fullscreen_article_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, _parameter| {
                let main_window = this.main_window();
                let is_fullscreened = main_window.is_fullscreened();
                main_window.set_fullscreened(!is_fullscreened);
                if let Some(article_navigation_page) = main_window.content_page().inner().content() {
                    article_navigation_page.set_can_pop(is_fullscreened);
                }
            }),
        );

        // -------------------------
        // export article
        // -------------------------
        let export_article_action = SimpleAction::new("export-article", None);
        export_article_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, _parameter| {
                this.export_article();
            }),
        );
        export_article_action.set_enabled(false);

        // -------------------------
        // open article in browser
        // -------------------------
        let open_selected_article_action = SimpleAction::new("open-selected-article-in-browser", None);
        open_selected_article_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, _parameter| {
                this.open_selected_article_in_browser();
            }),
        );
        open_selected_article_action.set_enabled(false);

        // -------------------------
        // open uri in browser
        // -------------------------
        let type_ = glib::VariantType::new("s").unwrap();
        let open_uri_action = SimpleAction::new("open-uri-in-browser", Some(&type_));
        open_uri_action.connect_activate(clone!(@weak self as this => @default-panic, move |_action, parameter| {
            if let Some(uri) = parameter.and_then(|p| p.str()) {
                if let Ok(uri) = Url::parse(uri) {
                    this.open_url_in_default_browser(&uri, false);
                }
            } else {
                App::default().in_app_notifiaction(&i18n("open uri: no parameter"));
            }
        }));

        let type_ = glib::VariantType::new("s").unwrap();
        let open_article_action = SimpleAction::new("open-article-in-browser", Some(&type_));
        open_article_action.connect_activate(clone!(@weak self as this => @default-panic, move |_action, parameter| {
            if let Some(id_str) = parameter.and_then(|p| p.str()) {
                let article_id = ArticleID::new(id_str);
                this.open_article_in_browser(&article_id);
            }
        }));

        // -------------------------
        // toggle article read
        // -------------------------
        let type_ = glib::VariantType::new("s").unwrap();
        let toggle_article_read_action = SimpleAction::new("toggle-article-read", Some(&type_));
        toggle_article_read_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, parameter| {
                if let Some(id_str) = parameter.and_then(|p| p.str()) {
                    let article_id = ArticleID::new(id_str);
                    this.toggle_article_read(&Arc::new(article_id));
                }
            }),
        );

        // -------------------------
        // toggle article marked
        // -------------------------
        let type_ = glib::VariantType::new("s").unwrap();
        let toggle_article_marked_action = SimpleAction::new("toggle-article-marked", Some(&type_));
        toggle_article_marked_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, parameter| {
                if let Some(id_str) = parameter.and_then(|p| p.str()) {
                    let article_id = ArticleID::new(id_str);
                    this.toggle_article_marked(&Arc::new(article_id));
                }
            }),
        );

        // -------------------------
        // show error dialog
        // -------------------------
        let show_error_dialog_action = SimpleAction::new("show-error-dialog", None);
        show_error_dialog_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, _parameter| {
                if let Some(error) = this.imp().news_flash_error.take() {
                    let _ = ErrorDialog::new(&error, Some(&this.main_window()));
                }
            }),
        );

        // ---------------------------
        // remove current undo action
        // ---------------------------
        let remove_undo_action = SimpleAction::new("remove-undo-action", None);
        remove_undo_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, _parameter| {
                this.main_window().content_page().remove_current_undo_action();
            }),
        );

        // ---------------------------
        // remove current undo action
        // ---------------------------
        let ignore_tls_errors_action = SimpleAction::new("ignore-tls-errors", None);
        ignore_tls_errors_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, _parameter| {
                this.ignore_tls_errors();
            }),
        );

        main_window.add_action(&delete_category_action);
        main_window.add_action(&edit_category_dialog_action);
        main_window.add_action(&delete_feed_action);
        main_window.add_action(&delete_tag_action);
        main_window.add_action(&edit_feed_dialog_action);
        main_window.add_action(&edit_tag_dialog_action);
        main_window.add_action(&show_shortcut_window_action);
        main_window.add_action(&show_about_window_action);
        main_window.add_action(&settings_window_action);
        main_window.add_action(&discover_dialog_action);
        main_window.add_action(&add_tag_action);
        main_window.add_action(&add_category_action);
        main_window.add_action(&add_feed_action);
        main_window.add_action(&quit_action);
        main_window.add_action(&import_opml_action);
        main_window.add_action(&export_opml_action);
        main_window.add_action(&save_image_action);
        main_window.add_action(&reset_account_action);
        main_window.add_action(&update_login_action);
        main_window.add_action(&close_article_action);
        main_window.add_action(&fullscreen_article_action);
        main_window.add_action(&open_selected_article_action);
        main_window.add_action(&open_uri_action);
        main_window.add_action(&open_article_action);
        main_window.add_action(&export_article_action);
        main_window.add_action(&toggle_article_read_action);
        main_window.add_action(&toggle_article_marked_action);
        main_window.add_action(&show_error_dialog_action);
        main_window.add_action(&remove_undo_action);
        main_window.add_action(&ignore_tls_errors_action);
    }

    pub fn update_sidebar(&self) {
        self.main_window().content_page().update_sidebar()
    }

    pub fn update_article_list(&self) {
        self.main_window().content_page().update_article_list()
    }

    pub fn update_article_header(&self) {
        self.main_window().update_article_header()
    }

    pub fn load_more_articles(&self) {
        self.main_window().content_page().load_more_articles()
    }

    pub fn in_app_error(&self, msg: &str, error: NewsFlashError) {
        self.main_window().content_page().newsflash_error(msg, error)
    }

    pub fn in_app_notifiaction(&self, msg: &str) {
        self.main_window().content_page().simple_message(msg);
    }

    pub fn dismiss_notifications(&self) {
        self.main_window().content_page().dismiss_notifications();
    }

    pub fn sync(&self) {
        self.main_window().content_page().article_list_column().start_sync();
        App::set_background_status(constants::BACKGROUND_SYNC);

        self.execute_with_callback(
            |news_flash, client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    let new_article_count = news_flash.sync(&client).await?;
                    let unread_total = news_flash.unread_count_all().unwrap_or(0);
                    let feed_ids = news_flash
                        .get_feeds()
                        .map(|(feeds, _mappings)| feeds.into_iter().map(|f| f.feed_id).collect::<HashSet<FeedID>>())
                        .unwrap_or_else(|_| HashSet::new());
                    Ok((new_article_count, unread_total, feed_ids))
                } else {
                    Err(NewsFlashError::NotLoggedIn)
                }
            },
            |app, res| {
                App::set_background_status(constants::BACKGROUND_IDLE);

                match res {
                    Ok((new_article_count, unread_total, feed_ids)) => {
                        _ = app.settings().borrow_mut().delete_old_feed_settings(&feed_ids);

                        app.main_window().content_page().article_list_column().finish_sync();
                        App::default().update_sidebar();
                        App::default().update_article_list();
                        let counts = NotificationCounts {
                            new: new_article_count,
                            unread: unread_total,
                        };
                        app.show_notification(counts);
                    }
                    Err(error) => {
                        app.main_window().content_page().article_list_column().finish_sync();
                        App::default().in_app_error(&i18n("Failed to sync"), error);
                    }
                }
            },
        );
    }

    fn init_sync(&self) {
        self.main_window().content_page().article_list_column().start_sync();

        // features might have changed after login
        // - check if discover is allowed
        // - update add popover features
        if let Some(discover_dialog_action) = self.main_window().lookup_action("discover") {
            discover_dialog_action
                .downcast::<SimpleAction>()
                .expect("downcast Action to SimpleAction")
                .set_enabled(self.features().contains(PluginCapabilities::ADD_REMOVE_FEEDS));
        }

        self.execute_with_callback(
            |news_flash, client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    let new_article_count = news_flash.initial_sync(&client).await?;
                    let unread_total = news_flash.unread_count_all().unwrap_or(0);
                    Ok((new_article_count, unread_total))
                } else {
                    Err(NewsFlashError::NotLoggedIn)
                }
            },
            |app, res| match res {
                Ok((new_article_count, unread_count)) => {
                    app.main_window().content_page().article_list_column().finish_sync();
                    App::default().update_sidebar();
                    App::default().update_article_list();
                    let counts = NotificationCounts {
                        new: new_article_count,
                        unread: unread_count,
                    };
                    app.show_notification(counts);
                }
                Err(error) => {
                    app.main_window().content_page().article_list_column().finish_sync();
                    App::default().in_app_error(&i18n("Failed to sync"), error);
                }
            },
        );
    }

    fn show_notification(&self, counts: NotificationCounts) {
        // don't notify when window is visible and focused
        let window = self.main_window();
        if window.is_visible() && window.is_active() {
            return;
        }

        if counts.new > 0 && counts.unread > 0 {
            let summary = i18n("New Articles");

            let message = if counts.new == 1 {
                i18n_f("There is 1 new article ({} unread)", &[&counts.unread.to_string()])
            } else {
                i18n_f(
                    "There are {} new articles ({} unread)",
                    &[&counts.new.to_string(), &counts.unread.to_string()],
                )
            };

            let notification = Notification::new(&summary);
            notification.set_body(Some(&message));
            notification.set_priority(NotificationPriority::Normal);
            notification.set_icon(&ThemedIcon::new(APP_ID));

            self.send_notification(Some("newsflash_sync"), &notification);
        }
    }

    pub fn main_window(&self) -> MainWindow {
        self.imp().window.borrow().clone().expect("window not initialized")
    }

    pub fn login(&self, data: LoginData) {
        let id = data.id();
        let user_api_secret = match &data {
            LoginData::OAuth(oauth_data) => oauth_data.custom_api_secret.clone(),
            _ => None,
        };

        let data_clone = data.clone();
        self.execute_with_callback(
            |news_flash, client| async move {
                let news_flash_lib = NewsFlash::new(&DATA_DIR, &CONFIG_DIR, &id, user_api_secret)?;
                news_flash_lib.login(data_clone, &client).await?;

                news_flash.write().await.replace(news_flash_lib);
                Ok(())
            },
            move |app, res: Result<(), NewsFlashError>| {
                if let Err(error) = res {
                    app.main_window().show_login_error(error, &data);
                } else {
                    app.update_features();

                    // show content page
                    app.main_window().show_content_page();

                    // schedule initial sync
                    app.init_sync();
                    app.schedule_sync();
                }
            },
        );
    }

    pub fn update_login(&self) {
        self.main_window().content_page().dismiss_notifications();
        self.execute_with_callback(
            |news_flash, _client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    news_flash.get_login_data().await
                } else {
                    None
                }
            },
            |app, res| {
                if let Some(login_data) = res {
                    let plugin_id = login_data.id();
                    match login_data {
                        LoginData::None(_id) => error!("updating login for local should never happen!"),
                        LoginData::Direct(_) | LoginData::OAuth(_) => {
                            app.main_window().show_login_page(&plugin_id, Some(login_data))
                        }
                    }
                }
            },
        );
    }

    pub fn reset_account(&self) {
        let news_flash = self.imp().news_flash.clone();
        self.execute_with_callback(
            |_news_flash, client| async move {
                if let Some(news_flash_lib) = news_flash.read().await.as_ref() {
                    news_flash_lib.logout(&client).await?;
                }
                news_flash.write().await.take();
                Ok(())
            },
            |app, res: Result<(), NewsFlashError>| match res {
                Ok(()) => {
                    app.main_window().content_page().clear();
                    app.main_window()
                        .content_page()
                        .articleview_column()
                        .show_article(None, None);
                    app.main_window().show_welcome_page()
                }
                Err(error) => {
                    app.main_window().reset_account_failed(error);
                }
            },
        );
    }

    pub fn schedule_sync(&self) {
        let imp = self.imp();
        GtkUtil::remove_source(imp.sync_source_id.take());
        let sync_interval = imp.settings.borrow().get_sync_interval();
        let sync_on_metered_connection = imp.settings.borrow().get_sync_on_metered();
        if let Some(sync_interval) = sync_interval.as_seconds() {
            imp.sync_source_id.borrow_mut().replace(glib::timeout_add_seconds_local(
                sync_interval,
                clone!(@weak self as this => @default-panic, move || {
                        let network_monitor = gio::NetworkMonitor::default();
                        let power_profile_monitor = gio::PowerProfileMonitor::get_default();

                        // check if on metered connection and only sync if chosen in preferences
                        // also only sync if power saving mode (low battery, etc.) is not enabled
                        if (!network_monitor.is_network_metered() || sync_on_metered_connection) &&
                           !power_profile_monitor.is_power_saver_enabled() {
                            this.sync();
                        }

                        ControlFlow::Continue
                }),
            ));
        } else {
            self.imp().sync_source_id.take();
        }
    }

    pub fn load_favicon(&self, feed_id: FeedID, oneshot_sender: OneShotSender<Option<FavIcon>>) {
        self.execute(move |news_flash, client| async move {
            if let Some(news_flash) = news_flash.read().await.as_ref() {
                let res = news_flash.get_icon(&feed_id, &client).await;
                let favicon = match res {
                    Ok(favicon) => Some(favicon),
                    Err(_) => {
                        warn!("Failed to load favicon for feed: '{}'", feed_id);
                        None
                    }
                };
                oneshot_sender.send(favicon).expect(CHANNEL_ERROR);
            }
        });
    }

    pub fn load_thumbnail(&self, article_id: &ArticleID, oneshot_sender: OneShotSender<Option<Thumbnail>>) {
        let article_id = article_id.clone();
        self.execute(move |news_flash, client| async move {
            if let Some(news_flash) = news_flash.read().await.as_ref() {
                let res = news_flash.get_article_thumbnail(&article_id, &client).await;
                let thumbnail = match res {
                    Ok(thumbnail) => Some(thumbnail),
                    Err(_) => {
                        log::debug!("Failed to load thumbnail for article: '{}'", article_id);
                        None
                    }
                };
                oneshot_sender.send(thumbnail).expect(CHANNEL_ERROR);
            }
        });
    }

    pub fn mark_article_read(&self, update: ReadUpdate) {
        self.main_window()
            .content_page()
            .article_list_column()
            .article_list()
            .set_article_row_state(&update.article_id, Some(update.read), None);

        let article_id_vec = vec![update.article_id.clone()];
        let read_status = update.read;

        self.execute_with_callback(
            move |news_flash, client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    news_flash.set_article_read(&article_id_vec, read_status, &client).await
                } else {
                    Err(NewsFlashError::NotLoggedIn)
                }
            },
            move |app, res| {
                if let Err(error) = res {
                    let message = i18n_f("Failed to mark article read: '{}'", &[&update.article_id.to_string()]);
                    error!("{message}");
                    App::default().in_app_error(&message, error);
                    App::default().update_article_list();
                }

                App::default().update_sidebar();
                let main_window = app.main_window();
                let article_column = main_window.content_page().articleview_column();

                let (visible_article, visible_article_enclosures) = article_column.article_view().get_visible_article();
                if let Some(mut visible_article) = visible_article {
                    if visible_article.article_id == update.article_id {
                        visible_article.unread = update.read;
                        article_column.show_article(Some(&visible_article), visible_article_enclosures.as_ref());
                        article_column
                            .article_view()
                            .update_visible_article(Some(visible_article.unread), None);
                    }
                }
            },
        );
    }

    fn mark_article(&self, update: MarkUpdate) {
        self.main_window()
            .content_page()
            .article_list_column()
            .article_list()
            .set_article_row_state(&update.article_id, None, Some(update.marked));

        let article_id_vec = vec![update.article_id.clone()];
        let mark_status = update.marked;

        self.execute_with_callback(
            move |news_flash, client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    news_flash
                        .set_article_marked(&article_id_vec, mark_status, &client)
                        .await
                } else {
                    Err(NewsFlashError::NotLoggedIn)
                }
            },
            move |app, res| {
                if let Err(error) = res {
                    let message = i18n_f("Failed to star article: '{}'", &[&update.article_id.to_string()]);
                    error!("{message}");
                    App::default().in_app_error(&message, error);
                    App::default().update_article_list();
                }

                App::default().update_sidebar();
                let main_window = app.main_window();
                let article_column = main_window.content_page().articleview_column();

                let (visible_article, visible_article_enclosures) = article_column.article_view().get_visible_article();
                if let Some(mut visible_article) = visible_article {
                    if visible_article.article_id == update.article_id {
                        visible_article.marked = update.marked;
                        article_column.show_article(Some(&visible_article), visible_article_enclosures.as_ref());
                        article_column
                            .article_view()
                            .update_visible_article(None, Some(visible_article.marked));
                    }
                }
            },
        );
    }

    pub fn toggle_selected_article_read(&self) {
        // get selected article from list
        let selected_article = self
            .main_window()
            .content_page()
            .article_list_column()
            .article_list()
            .get_selected_article_model();

        let update = if let Some(selected_article) = selected_article {
            Some(ReadUpdate {
                article_id: selected_article.article_id(),
                read: selected_article.read().invert().into(),
            })
        } else {
            // if no selected article in list check article view next
            let (article, _enclosure) = self
                .main_window()
                .content_page()
                .articleview_column()
                .article_view()
                .get_visible_article();

            article.map(|article| ReadUpdate {
                article_id: article.article_id.clone(),
                read: article.unread.invert(),
            })
        };

        if let Some(update) = update {
            self.mark_article_read(update);
        }
    }

    fn toggle_article_read(&self, article_id: &ArticleID) {
        if let Some(article_model) = self
            .main_window()
            .content_page()
            .article_list_column()
            .article_list()
            .get_model(article_id)
        {
            let update = ReadUpdate {
                article_id: article_id.clone(),
                read: article_model.read().invert().into(),
            };

            self.mark_article_read(update);
        } else {
            log::warn!("article '{:?}' not part of article-list", article_id);
        }
    }

    pub fn toggle_selected_article_marked(&self) {
        // get selected article from list
        let selected_article = self
            .main_window()
            .content_page()
            .article_list_column()
            .article_list()
            .get_selected_article_model();

        let update = if let Some(selected_article) = selected_article {
            Some(MarkUpdate {
                article_id: selected_article.article_id(),
                marked: selected_article.marked().invert().into(),
            })
        } else {
            // if no selected article in list check article view next
            let (article, _enclosure) = self
                .main_window()
                .content_page()
                .articleview_column()
                .article_view()
                .get_visible_article();

            article.map(|article| MarkUpdate {
                article_id: article.article_id.clone(),
                marked: article.marked.invert(),
            })
        };

        if let Some(update) = update {
            self.mark_article(update);
        }
    }

    fn toggle_article_marked(&self, article_id: &ArticleID) {
        if let Some(article_model) = self
            .main_window()
            .content_page()
            .article_list_column()
            .article_list()
            .get_model(article_id)
        {
            let update = MarkUpdate {
                article_id: article_id.clone(),
                marked: article_model.marked().invert().into(),
            };

            self.mark_article(update);
        } else {
            log::warn!("article '{:?}' not part of article-list", article_id);
        }
    }

    pub fn spawn_shortcut_window(&self) {
        let dialog = ShortcutsDialog::new(&self.main_window(), &self.imp().settings.borrow());
        dialog.widget.present();
    }

    pub fn spawn_settings_window(&self) {
        SettingsDialog::new(&self.main_window()).present();
    }

    pub fn spawn_discover_dialog(&self) {
        DiscoverDialog::new(&self.main_window()).present();
    }

    pub fn add_feed(&self, feed_url: Url, title: Option<String>, category_id: Option<CategoryID>) {
        info!("add feed '{}'", feed_url);

        self.execute_with_callback(
            |news_flash, client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    news_flash.add_feed(&feed_url, title, category_id, &client).await
                } else {
                    Err(NewsFlashError::NotLoggedIn)
                }
            },
            |_app, res| match res {
                Ok(_) => {
                    App::default().update_sidebar();
                }
                Err(error) => {
                    error!("Failed to add feed: {error}");
                    App::default().in_app_error(&i18n("Failed to add feed"), error);
                }
            },
        );
    }

    pub fn add_category(&self, title: String) {
        info!("add category '{}'", title);

        self.execute_with_callback(
            |news_flash, client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    news_flash.add_category(&title, None, &client).await
                } else {
                    Err(NewsFlashError::NotLoggedIn)
                }
            },
            |app, res| {
                if let Err(error) = res {
                    error!("Failed to add category: {error}");
                    app.in_app_error(&i18n("Failed to add category"), error);
                } else {
                    app.update_sidebar();
                }
            },
        );
    }

    pub fn add_tag(&self, color: String, title: String, assign_to_article: Option<ArticleID>) {
        info!("add tag '{}'", title);

        self.execute_with_callback(
            |news_flash, client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    news_flash.add_tag(&title, Some(color), &client).await
                } else {
                    Err(NewsFlashError::NotLoggedIn)
                }
            },
            move |app, res| match res {
                Ok(tag) => {
                    if let Some(article_id) = assign_to_article {
                        app.tag_article(article_id, tag.tag_id);
                    }
                    app.update_sidebar();
                    app.update_article_header();
                }
                Err(error) => {
                    error!("Failed to add tag: {error}");
                    app.in_app_error(&i18n("Failed to add tag"), error);
                }
            },
        );
    }

    pub fn rename_feed(&self, feed: &Feed, new_title: &str) {
        let feed = feed.clone();
        let feed_id = feed.feed_id.clone();
        let new_title = new_title.to_string();
        self.execute_with_callback(
            |news_flash, client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    news_flash.rename_feed(&feed, &new_title, &client).await
                } else {
                    Err(NewsFlashError::NotLoggedIn)
                }
            },
            move |app, res| match res {
                Ok(_feed) => {
                    app.update_article_list();
                    app.update_sidebar();

                    // if the visible article belongs to the renamed feed
                    // show it again to gather the updated feed name
                    if let (Some(fat_article), _enclosure) = app
                        .main_window()
                        .content_page()
                        .articleview_column()
                        .article_view()
                        .get_visible_article()
                    {
                        if fat_article.feed_id == feed_id {
                            app.main_window().show_article(fat_article.article_id);
                        }
                    }
                }
                Err(error) => {
                    error!("Failed to rename feed: {error}");
                    app.in_app_error(&i18n("Failed to rename feed"), error);
                }
            },
        );
    }

    pub fn rename_category(&self, category_id: &CategoryID, new_title: &str) {
        let category_id = category_id.clone();
        let new_title = new_title.to_string();

        self.execute_with_callback(
            |news_flash, client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    news_flash.rename_category(&category_id, &new_title, &client).await
                } else {
                    Err(NewsFlashError::NotLoggedIn)
                }
            },
            move |_app, res| match res {
                Ok(_feed) => {
                    App::default().update_sidebar();
                }
                Err(error) => {
                    error!("Failed to rename category: {error}");
                    App::default().in_app_error(&i18n("Failed to rename category"), error);
                }
            },
        );
    }

    pub fn edit_tag(&self, tag: &Tag, new_title: &str, new_color: &Option<String>) {
        let tag = tag.clone();
        let new_title = new_title.to_string();
        let new_color = new_color.clone();

        self.execute_with_callback(
            |news_flash, client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    news_flash.edit_tag(&tag, &new_title, &new_color, &client).await
                } else {
                    Err(NewsFlashError::NotLoggedIn)
                }
            },
            move |_app, res| match res {
                Ok(_tag) => {
                    App::default().update_article_list();
                    App::default().update_sidebar();
                }
                Err(error) => {
                    let message = &i18n("Failed to tag article");
                    error!("{message}");
                    App::default().in_app_error(message, error);
                }
            },
        );
    }

    pub fn set_feed_list_order(&self, order: GFeedOrder) {
        self.execute_with_callback(
            move |news_flash, _client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    if order == GFeedOrder::Alphabetical {
                        news_flash.sort_alphabetically().await
                    } else {
                        Ok(())
                    }
                } else {
                    Err(NewsFlashError::NotLoggedIn)
                }
            },
            move |app, res| {
                if let Err(error) = res {
                    App::default().in_app_error(&i18n("Failed to set feed list order to manual"), error);
                }
                _ = app.settings().borrow_mut().set_feed_list_order(order);
                App::default().update_sidebar();
            },
        );
    }

    pub fn move_feed(&self, from: FeedMapping, to: FeedMapping) {
        if App::default().settings().borrow().get_feed_list_order() == GFeedOrder::Alphabetical {
            _ = App::default()
                .settings()
                .borrow_mut()
                .set_feed_list_order(GFeedOrder::Manual);
        }

        self.execute_with_callback(
            |news_flash, client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    news_flash.move_feed(&from, &to, &client).await
                } else {
                    Err(NewsFlashError::NotLoggedIn)
                }
            },
            move |_app, res| match res {
                Ok(()) => {
                    App::default().update_sidebar();
                }
                Err(error) => {
                    error!("Failed to move feed: {error}");
                    App::default().in_app_error(&i18n("Failed to move feed"), error);
                }
            },
        );
    }

    pub fn move_category(&self, to: CategoryMapping) {
        if App::default().settings().borrow().get_feed_list_order() == GFeedOrder::Alphabetical {
            _ = App::default()
                .settings()
                .borrow_mut()
                .set_feed_list_order(GFeedOrder::Manual);
        }

        self.execute_with_callback(
            |news_flash, client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    news_flash.move_category(&to, &client).await
                } else {
                    Err(NewsFlashError::NotLoggedIn)
                }
            },
            move |_app, res| match res {
                Ok(()) => {
                    App::default().update_sidebar();
                }
                Err(error) => {
                    error!("Failed to move category: {error}");
                    App::default().in_app_error(&i18n("Failed to move category"), error);
                }
            },
        );
    }

    pub fn delete_feed(&self, feed_id: FeedID, callback: Box<dyn Fn()>) {
        self.execute_with_callback(
            |news_flash, client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    news_flash.remove_feed(&feed_id, &client).await
                } else {
                    Err(NewsFlashError::NotLoggedIn)
                }
            },
            move |_app, res| {
                if let Err(error) = res {
                    error!("Failed to delete feed: {error}");
                    App::default().in_app_error(&i18n("Failed to delete feed"), error);
                }

                callback();
            },
        );
    }

    pub fn delete_category(&self, category_id: CategoryID, callback: Box<dyn Fn()>) {
        self.execute_with_callback(
            |news_flash, client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    news_flash.remove_category(&category_id, true, &client).await
                } else {
                    Err(NewsFlashError::NotLoggedIn)
                }
            },
            move |_app, res| {
                if let Err(error) = res {
                    error!("Failed to delete category: {error}");
                    App::default().in_app_error(&i18n("Failed to delete category"), error);
                }

                callback();
            },
        );
    }

    pub fn delete_tag(&self, tag_id: TagID, callback: Box<dyn Fn()>) {
        self.execute_with_callback(
            |news_flash, client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    news_flash.remove_tag(&tag_id, &client).await
                } else {
                    Err(NewsFlashError::NotLoggedIn)
                }
            },
            move |app, res| {
                if let Err(error) = res {
                    error!("Failed to delete tag: {error}");
                    App::default().in_app_error(&i18n("Failed to delete tag"), error);
                } else {
                    app.update_article_list();
                }

                callback();
            },
        );
    }

    pub fn tag_article(&self, article_id: ArticleID, tag_id: TagID) {
        let article_id_clone = article_id.clone();

        self.execute_with_callback(
            |news_flash, client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    let (tags, _taggings) = news_flash.get_tags()?;

                    let tag = tags
                        .into_iter()
                        .find(|t| t.tag_id == tag_id)
                        .ok_or(NewsFlashError::Unknown)?;
                    info!("tag article '{}' with '{}'", article_id, tag.tag_id);
                    news_flash.tag_article(&article_id, &tag_id, &client).await?;

                    Ok(tag)
                } else {
                    Err(NewsFlashError::NotLoggedIn)
                }
            },
            move |app, res| {
                match res {
                    Ok(tag) => {
                        app.main_window()
                            .content_page()
                            .article_list_column()
                            .article_list()
                            .article_row_update_tags(&article_id_clone, Some(&tag), None);
                    }
                    Err(error) => {
                        error!("Failed to tag article: {}", error);
                        App::default().in_app_error(&i18n("Failed to tag article"), error);
                    }
                };
            },
        );
    }

    pub fn untag_article(&self, article_id: ArticleID, tag_id: TagID) {
        let article_id_clone = article_id.clone();

        self.execute_with_callback(
            |news_flash, client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    let (tags, _taggings) = news_flash.get_tags()?;

                    let tag = tags
                        .into_iter()
                        .find(|t| t.tag_id == tag_id)
                        .ok_or(NewsFlashError::Unknown)?;
                    info!("untag article '{}' remove '{}'", article_id, tag.tag_id);
                    news_flash.untag_article(&article_id, &tag_id, &client).await?;

                    Ok(tag)
                } else {
                    Err(NewsFlashError::NotLoggedIn)
                }
            },
            move |app, res| {
                match res {
                    Ok(tag) => {
                        app.main_window()
                            .content_page()
                            .article_list_column()
                            .article_list()
                            .article_row_update_tags(&article_id_clone, None, Some(&tag));
                    }
                    Err(error) => {
                        error!("Failed to untag article: {}", error);
                        App::default().in_app_error(&i18n("Failed to untag article"), error);
                    }
                };
            },
        );
    }

    fn export_article(&self) {
        let article = match self
            .main_window()
            .content_page()
            .articleview_column()
            .article_view()
            .get_visible_article()
        {
            (Some(article), _article_enclosures) => article,
            _ => return,
        };
        let title = article.title.clone();

        let (article_tx, article_rx) =
            oneshot::channel::<Result<(news_flash::models::FatArticle, news_flash::models::Feed), NewsFlashError>>();
        let window_state = self.main_window().content_page().state().borrow().clone();

        self.execute(|news_flash, client| async move {
            if let Some(news_flash) = news_flash.read().await.as_ref() {
                let article = if window_state.get_offline() {
                    article
                } else {
                    let article_res = news_flash
                        .article_download_images(&article.article_id, &client, None)
                        .await;
                    match article_res {
                        Ok(article) => article,
                        Err(error) => {
                            article_tx.send(Err(error)).expect(CHANNEL_ERROR);
                            return;
                        }
                    }
                };

                let feeds_res = news_flash.get_feeds();
                let feeds = match feeds_res {
                    Ok((feeds, _mappings)) => feeds,
                    Err(error) => {
                        article_tx.send(Err(error)).expect(CHANNEL_ERROR);
                        return;
                    }
                };

                let feed = match feeds.into_iter().find(|f| f.feed_id == article.feed_id) {
                    Some(feed) => feed,
                    None => {
                        article_tx.send(Err(NewsFlashError::Unknown)).expect(CHANNEL_ERROR);
                        return;
                    }
                };

                article_tx.send(Ok((article, feed))).expect(CHANNEL_ERROR);
            }
        });

        let main_window = self.main_window();
        Util::glib_spawn_future(async move {
            let filter = FileFilter::new();
            filter.add_pattern("*.html");
            filter.add_mime_type("text/html");
            filter.set_name(Some("HTML"));

            let filter_list = ListStore::new::<FileFilter>();
            filter_list.append(&filter);

            let dialog = FileDialog::builder()
                .accept_label(i18n("_Save"))
                .title(i18n("Export Article"))
                .modal(true)
                .initial_folder(&gio::File::for_path(glib::home_dir()))
                .filters(&filter_list)
                .build();

            if let Some(title) = &title {
                dialog.set_initial_name(Some(&format!("{}.html", title.replace('/', "_"))));
            } else {
                dialog.set_initial_name(Some("Article.html"));
            }

            let (dialog_tx, dialog_rx) = oneshot::channel::<Option<gio::File>>();
            let dialog_tx = RefCell::new(Some(dialog_tx));

            dialog.save(
                Some(&main_window),
                None::<&gio::Cancellable>,
                move |response| match response {
                    Ok(file) => {
                        if let Some(dialog_tx) = dialog_tx.take() {
                            dialog_tx.send(Some(file)).unwrap();
                        }
                    }
                    Err(error) => {
                        App::default().in_app_notifiaction(&i18n_f("No file set: {}", &[&error.to_string()]));
                    }
                },
            );

            let file = if let Ok(Some(file)) = dialog_rx.await {
                file
            } else {
                return;
            };

            main_window
                .content_page()
                .articleview_column()
                .start_more_actions_spinner();

            let prefer_scraped_content = App::default()
                .content_page_state()
                .borrow()
                .get_prefer_scraped_content();

            let article_result = article_rx.await.expect(CHANNEL_ERROR);
            let (article, feed) = match article_result {
                Ok((article, feed)) => (article, feed),
                Err(error) => {
                    error!("Failed to download images: {error}");
                    App::default().in_app_error(&i18n("Failed to download images"), error);
                    return;
                }
            };

            let html = ArticleView::build_article_static(&article, &feed.label, Some(true), prefer_scraped_content);
            if let Err(error) = GtkUtil::write_bytes_to_file(html.as_bytes(), &file) {
                App::default().in_app_notifiaction(&error.to_string());
            }

            main_window
                .content_page()
                .articleview_column()
                .stop_more_actions_spinner();
        });
    }

    pub fn grab_article_content(&self) {
        let article = match self
            .main_window()
            .content_page()
            .articleview_column()
            .article_view()
            .get_visible_article()
        {
            (Some(article), _article_enclosures) => article,
            _ => return,
        };

        // Article already scraped: just swap to scraped content
        if article.scraped_content.is_some() {
            self.main_window()
                .content_page()
                .state()
                .borrow_mut()
                .set_prefer_scraped_content(true);
            self.main_window()
                .content_page()
                .articleview_column()
                .article_view()
                .redraw_article();
            return;
        }

        self.main_window()
            .content_page()
            .state()
            .borrow_mut()
            .started_scraping_article();
        self.main_window()
            .content_page()
            .articleview_column()
            .start_scrap_content_spinner();

        let article_id = article.article_id;
        self.execute_with_callback(
            |news_flash, client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    let news_flash_future = news_flash.article_scrap_content(&article_id, &client, None);
                    timeout(Duration::from_secs(60), news_flash_future).await
                } else {
                    Ok(Err(NewsFlashError::NotLoggedIn))
                }
            },
            |app, res| {
                app.main_window()
                    .content_page()
                    .articleview_column()
                    .stop_scrap_content_spinner();

                app.main_window()
                    .content_page()
                    .state()
                    .borrow_mut()
                    .finished_scraping_article();

                app.main_window()
                    .content_page()
                    .articleview_column()
                    .update_scrape_content_button_state(false);

                match res {
                    Ok(Ok(article)) => {
                        let scraped_article_id = article.article_id;
                        let visible_article_id = app
                            .main_window()
                            .content_page()
                            .articleview_column()
                            .article_view()
                            .get_visible_article()
                            .0
                            .map(|a| a.article_id);

                        if let Some(visible_article_id) = visible_article_id {
                            if scraped_article_id == visible_article_id {
                                app.main_window().show_article(scraped_article_id);
                            }
                        }

                        if article.thumbnail_url.is_some() {
                            app.update_article_list();
                        }
                    }
                    Ok(Err(error)) => {
                        log::warn!("Internal scraper failed: {error}");
                        App::default().in_app_error(&i18n("Scraper failed to extract content"), error);
                    }
                    Err(_error) => {
                        log::warn!("Internal scraper elapsed");
                        App::default().in_app_notifiaction(&i18n("Scraper Timeout"));
                    }
                }
            },
        );
    }

    fn import_opml(&self) {
        let future = async move {
            let filter = FileFilter::new();
            filter.add_pattern("*.OPML");
            filter.add_pattern("*.opml");
            filter.add_mime_type("application/xml");
            filter.add_mime_type("text/xml");
            filter.add_mime_type("text/x-opml");
            filter.set_name(Some("OPML"));

            let filter_list = ListStore::new::<FileFilter>();
            filter_list.append(&filter);

            let dialog = FileDialog::builder()
                //.transient_for(&App::default().main_window())
                .accept_label(i18n("_Open"))
                .title(i18n("Import OPML"))
                .modal(true)
                .initial_folder(&gio::File::for_path(glib::home_dir()))
                .filters(&filter_list)
                .build();

            let (tx, rx) = oneshot::channel::<Option<String>>();
            let tx = RefCell::new(Some(tx));

            dialog.open(
                Some(&App::default().main_window()),
                None::<&gio::Cancellable>,
                move |response| {
                    if let Some(tx) = tx.take() {
                        if let Ok(file) = response {
                            let buffer = match GtkUtil::read_bytes_from_file(&file) {
                                Ok(buffer) => buffer,
                                Err(error) => {
                                    App::default().in_app_notifiaction(&error.to_string());
                                    tx.send(None).unwrap();
                                    return;
                                }
                            };

                            let opml_content = match String::from_utf8(buffer) {
                                Ok(string) => string,
                                Err(error) => {
                                    App::default().in_app_notifiaction(&i18n_f(
                                        "Failed read OPML string: {}",
                                        &[&error.to_string()],
                                    ));
                                    tx.send(None).unwrap();
                                    return;
                                }
                            };

                            tx.send(Some(opml_content)).unwrap();
                        }
                    }
                },
            );

            let opml_content = if let Ok(Some(opml_content)) = rx.await {
                opml_content
            } else {
                return;
            };

            App::default()
                .main_window()
                .content_page()
                .article_list_column()
                .start_sync();

            App::default().execute_with_callback(
                |news_flash, client| async move {
                    if let Some(news_flash) = news_flash.read().await.as_ref() {
                        news_flash.import_opml(&opml_content, false, &client).await
                    } else {
                        Err(NewsFlashError::NotLoggedIn)
                    }
                },
                |app, res| {
                    if let Err(error) = res {
                        App::default().in_app_error(&i18n("Failed to import OPML"), error);
                    } else {
                        App::default().update_sidebar();
                    }
                    app.main_window().content_page().article_list_column().finish_sync();
                },
            );
        };
        Util::glib_spawn_future(future);
    }

    fn export_opml(&self) {
        let main_window = self.main_window();
        let future = async move {
            let filter = FileFilter::new();
            filter.add_pattern("*.OPML");
            filter.add_pattern("*.opml");
            filter.add_mime_type("application/xml");
            filter.add_mime_type("text/xml");
            filter.add_mime_type("text/x-opml");
            filter.set_name(Some("OPML"));

            let filter_list = ListStore::new::<FileFilter>();
            filter_list.append(&filter);

            let dialog = FileDialog::builder()
                .accept_label(i18n("_Save"))
                .title(i18n("Export OPML"))
                .modal(true)
                .initial_folder(&gio::File::for_path(glib::home_dir()))
                .initial_name("NewsFlash.OPML")
                .filters(&filter_list)
                .build();

            let (tx, rx) = oneshot::channel::<Option<gio::File>>();
            let tx = RefCell::new(Some(tx));

            dialog.save(
                Some(&main_window),
                None::<&gio::Cancellable>,
                move |response| match response {
                    Ok(file) => {
                        if let Some(tx) = tx.take() {
                            tx.send(Some(file)).unwrap();
                        }
                    }
                    Err(_error) => {
                        App::default().in_app_notifiaction(&i18n("No file set."));
                    }
                },
            );

            let file = if let Ok(Some(file)) = rx.await {
                file
            } else {
                return;
            };

            App::default().execute_with_callback(
                |news_flash, _client| async move {
                    if let Some(news_flash) = news_flash.read().await.as_ref() {
                        news_flash.export_opml().await
                    } else {
                        Err(NewsFlashError::NotLoggedIn)
                    }
                },
                move |_app, res| {
                    let opml = match res {
                        Ok(opml) => opml,
                        Err(error) => {
                            App::default().in_app_error(&i18n("Failed to get OPML data"), error);
                            return;
                        }
                    };

                    // Format XML
                    if let Ok(tree) = xmltree::Element::parse(opml.as_bytes()) {
                        let write_config = xmltree::EmitterConfig::new().perform_indent(true);
                        let data = std::rc::Rc::new(std::cell::RefCell::new(Some(Vec::new())));
                        let rc_writer = rc_writer::RcOptionWriter::new(data.clone());
                        if tree.write_with_config(rc_writer, write_config).is_err() {
                            App::default().in_app_notifiaction(&i18n("Failed to write OPML data to disc"));
                        }
                        let data = data.borrow_mut().take().expect("FIXME");
                        if let Err(error) = GtkUtil::write_bytes_to_file(&data, &file) {
                            App::default().in_app_notifiaction(&error.to_string());
                        }
                    } else {
                        App::default().in_app_notifiaction(&i18n("Failed to parse OPML data for formatting"));
                    }
                },
            );
        };

        Util::glib_spawn_future(future);
    }

    fn save_image(&self, image_uri_str: &str) {
        let image_url = match Url::parse(image_uri_str) {
            Ok(url) => url,
            _ => return,
        };

        let segments = match image_url.path_segments() {
            Some(segments) => segments,
            _ => return,
        };

        let file_name: String = match segments.last() {
            Some(last_segment) => last_segment.into(),
            None => "image.jpeg".into(),
        };

        let main_window = self.main_window();
        let image_uri_str = image_uri_str.to_owned();
        let future = async move {
            let filter = FileFilter::new();
            filter.add_pattern("*.jpg");
            filter.add_pattern("*.jpeg");
            filter.add_pattern("*.png");
            filter.add_mime_type("image/jpeg");
            filter.add_mime_type("image/png");
            filter.set_name(Some("Image"));

            let filter_list = ListStore::new::<FileFilter>();
            filter_list.append(&filter);

            let dialog = FileDialog::builder()
                .accept_label(i18n("_Save"))
                .title(i18n("Save Image"))
                .modal(true)
                .initial_folder(&gio::File::for_path(glib::home_dir()))
                .initial_name(&file_name)
                .filters(&filter_list)
                .build();

            let (tx, rx) = oneshot::channel::<Option<gio::File>>();
            let tx = RefCell::new(Some(tx));

            dialog.save(
                Some(&main_window),
                None::<&gio::Cancellable>,
                move |response| match response {
                    Ok(file) => {
                        if let Some(tx) = tx.take() {
                            tx.send(Some(file)).unwrap();
                        }
                    }
                    Err(_error) => {
                        App::default().in_app_notifiaction(&i18n("No file set"));
                    }
                },
            );

            let file = if let Ok(Some(file)) = rx.await {
                file
            } else {
                return;
            };

            let uri_clone = image_uri_str.clone();
            App::default().execute_with_callback(
                |_news_flash, client| async move {
                    let bytes = client.get(&image_uri_str).send().await?.bytes().await?;
                    Ok(bytes)
                },
                move |_app, res: Result<_, reqwest::Error>| {
                    if let Ok(image_bytes) = res {
                        if let Err(error) = GtkUtil::write_bytes_to_file(&image_bytes, &file) {
                            App::default().in_app_notifiaction(&error.to_string());
                        }
                    } else {
                        App::default().in_app_notifiaction(&i18n_f("Failed to download image {}", &[&uri_clone]));
                    }
                },
            );
        };

        Util::glib_spawn_future(future);
    }

    pub fn queue_quit(&self) {
        self.imp().shutdown_in_progress.replace(true);
        self.main_window().close();
        self.main_window().content_page().execute_pending_undoable_action();

        // wait for ongoing sync to finish, but limit waiting to max 3s
        let start_wait_time = time::SystemTime::now();
        let max_wait_time = time::Duration::from_secs(3);
        self.wait_for_sync(start_wait_time, max_wait_time);
    }

    fn wait_for_sync(&self, start_wait_time: time::SystemTime, max_wait_time: time::Duration) {
        self.execute_with_callback(
            |news_flash, _client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    news_flash.is_sync_ongoing().await
                } else {
                    false
                }
            },
            move |app, is_sync_ongoing| {
                if is_sync_ongoing && start_wait_time.elapsed().expect("shutdown timer elapsed error") < max_wait_time {
                    glib::MainContext::default().iteration(true);
                    app.wait_for_sync(start_wait_time, max_wait_time);
                } else {
                    app.force_quit();
                }
            },
        );
    }

    fn force_quit(&self) {
        info!("Shutdown!");
        self.harvest_restore_relevant_state();

        if let Err(error) = self.main_window().content_page().state().borrow().write() {
            log::error!("Failed to serialize content page state: {error}");
        }
        self.quit();
    }

    fn harvest_restore_relevant_state(&self) {
        let state = self.content_page_state();
        let mut content_page_state_guard = state.borrow_mut();
        content_page_state_guard.set_window_size(self.main_window().default_size());
        content_page_state_guard.set_maximized(self.main_window().is_maximized());
        content_page_state_guard.set_article_view_visible(self.main_window().content_page().inner().shows_content());
        content_page_state_guard.set_article_view_zoom(
            self.main_window()
                .content_page()
                .articleview_column()
                .article_view()
                .get_zoom(),
        );
    }

    pub fn request_background_permission(autostart: bool) {
        let ctx = glib::MainContext::default();
        ctx.spawn_local_with_priority(Priority::LOW, async move {
            if !ashpd::is_sandboxed().await {
                return;
            }

            let response = ashpd::desktop::background::Background::request()
                .identifier(ashpd::WindowIdentifier::default())
                .reason(constants::BACKGROUND_IDLE)
                .auto_start(autostart)
                .send()
                .await
                .and_then(|request| request.response());

            if let Err(error) = response {
                log::error!("Requesting background permission failed: {error}");
            }

            App::set_background_status(constants::BACKGROUND_IDLE);
        });
    }

    pub fn set_background_status(message: &'static str) {
        glib::MainContext::default().spawn_local(async move {
            if !ashpd::is_sandboxed().await {
                return;
            }

            let proxy = ashpd::desktop::background::BackgroundProxy::new().await;
            if let Ok(proxy) = proxy {
                if let Err(error) = proxy.set_status(message).await {
                    log::error!("Failed to set background message: {error}");
                }
            }
        });
    }

    pub fn set_offline(&self, offline: bool) {
        if self.main_window().content_page().state().borrow().was_online() {
            self.dismiss_notifications();
            if offline {
                self.in_app_notifiaction(&i18n("NewsFlash is offline"));
            } else {
                self.in_app_notifiaction(&i18n("NewsFlash is online"));
            }
        }

        self.execute_with_callback(
            move |news_flash, client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    news_flash.set_offline(offline, &client).await
                } else {
                    Err(NewsFlashError::Offline)
                }
            },
            move |app, res| {
                if let Err(error) = res {
                    if offline {
                        app.in_app_error(&i18n("Failed to set offline"), error);
                    } else {
                        app.in_app_error(&i18n("Failed to apply changes made while offline"), error);
                    }
                }

                app.main_window().content_page().set_offline(offline);

                if let Some(import_opml_action) = app.main_window().lookup_action("import-opml") {
                    import_opml_action
                        .downcast::<SimpleAction>()
                        .expect("downcast Action to SimpleAction")
                        .set_enabled(!offline);
                }
                if let Some(discover_action) = app.main_window().lookup_action("discover") {
                    discover_action
                        .downcast::<SimpleAction>()
                        .expect("downcast Action to SimpleAction")
                        .set_enabled(!offline);
                }
            },
        );
    }

    fn ignore_tls_errors(&self) {
        let res1 = self.imp().settings.borrow_mut().set_accept_invalid_certs(true);
        let res2 = self.imp().settings.borrow_mut().set_accept_invalid_hostnames(true);
        if res1.is_err() || res2.is_err() {
            App::default().in_app_notifiaction(&i18n("Error writing settings"));
        }
    }

    pub fn copy_selected_article_url_to_clipboard(&self) {
        let article_model = self
            .main_window()
            .content_page()
            .articleview_column()
            .article_view()
            .get_visible_article();

        if let (Some(article_model), _article_enclosures) = article_model {
            if let Some(url) = article_model.url {
                self.main_window().clipboard().set_text(url.as_str());
            } else {
                warn!("Copy article url to clipboard: No url available.")
            }
        } else {
            warn!("Copy article url to clipboard: No article Selected.")
        }
    }

    pub fn open_selected_article_in_browser(&self) {
        let article_model = self
            .main_window()
            .content_page()
            .articleview_column()
            .article_view()
            .get_visible_article();

        if let (Some(article_model), _article_enclosures) = article_model {
            if let Some(url) = article_model.url {
                self.open_url_in_default_browser(&url, false);
            } else {
                warn!("Open selected article in browser: No url available.")
            }
        } else {
            warn!("Open selected article in browser: No article Selected.")
        }
    }

    fn open_article_in_browser(&self, article_id: &ArticleID) {
        let article_id = article_id.clone();
        self.execute_with_callback(
            |news_flash, _client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    news_flash.get_article(&article_id)
                } else {
                    Err(NewsFlashError::NotLoggedIn)
                }
            },
            |app, res| {
                if let Ok(article) = res {
                    if let Some(url) = &article.url {
                        app.open_url_in_default_browser(url, false)
                    }
                }
            },
        );
    }

    pub fn open_url_in_default_browser(&self, url: &Url, ask: bool) {
        let url = url.to_owned();

        let ctx = glib::MainContext::default();
        ctx.spawn_local(async move {
            if let Err(error) = OpenFileRequest::default().ask(ask).send_uri(&url).await {
                App::default().in_app_notifiaction(&i18n_f("Failed read URL: {}", &[&error.to_string()]));
            }
        });
    }
}
