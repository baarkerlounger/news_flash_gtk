mod article;
mod article_gobject;
mod article_update_msg;

pub use article::ArticleListArticleModel;
pub use article_gobject::{ArticleGObject, GDateTime, GMarked, GRead, GTags, GThumbnail};
pub use article_update_msg::{MarkUpdate, ReadUpdate};
use diffus::{edit::Edit, Diffable};
use log::warn;
use news_flash::models::{Article, ArticleID, ArticleOrder, Feed, Marked, Read, Tag};
use std::collections::HashSet;

#[derive(Debug)]
pub struct ArticleListModel {
    models: Vec<ArticleListArticleModel>,
    ids: HashSet<ArticleID>,
    sort: ArticleOrder,
}

impl ArticleListModel {
    pub fn new(sort: &ArticleOrder) -> Self {
        ArticleListModel {
            models: Vec::new(),
            ids: HashSet::new(),
            sort: sort.clone(),
        }
    }

    pub fn order(&self) -> ArticleOrder {
        self.sort.clone()
    }

    pub fn add(&mut self, articles: Vec<(Article, Option<&Feed>, Vec<&Tag>)>) {
        for (article, feed, tags) in articles {
            if self.contains(&article.article_id) {
                warn!("Listmodel already contains id {}", article.article_id);
                continue;
            }
            self.ids.insert(article.article_id.clone());
            self.models.push(ArticleListArticleModel::new(article, feed, tags));
        }
        self.sort();
    }

    pub fn add_model(&mut self, model: ArticleListArticleModel) {
        if self.contains(&model.id) {
            warn!("Listmodel already contains id {}", model.id);
            return;
        }
        self.ids.insert(model.id.clone());
        self.models.push(model);
        self.sort();
    }

    pub fn add_models(&mut self, models: Vec<ArticleListArticleModel>) {
        for model in models {
            if self.contains(&model.id) {
                warn!("Listmodel already contains id {}", model.id);
                continue;
            }
            self.ids.insert(model.id.clone());
            self.models.push(model);
        }
        self.sort();
    }

    pub fn contains(&self, article_id: &ArticleID) -> bool {
        self.ids.contains(article_id)
    }

    pub fn len(&self) -> usize {
        self.models.len()
    }

    pub fn models(&self) -> &Vec<ArticleListArticleModel> {
        &self.models
    }

    pub fn set_read(&mut self, id: &ArticleID, read: Read) {
        if !self.contains(id) {
            return;
        }

        if let Some(article_model) = self.models.iter_mut().find(|a| &a.id == id) {
            article_model.read = read;
        }
    }

    pub fn set_marked(&mut self, id: &ArticleID, marked: Marked) {
        if !self.contains(id) {
            return;
        }

        if let Some(article_model) = self.models.iter_mut().find(|a| &a.id == id) {
            article_model.marked = marked;
        }
    }

    pub fn add_tag(&mut self, id: &ArticleID, tag: &Tag) -> Option<&Vec<Tag>> {
        if !self.contains(id) {
            return None;
        }

        if let Some(article_model) = self.models.iter_mut().find(|a| &a.id == id) {
            article_model.tags.push(tag.clone());
            Some(&article_model.tags)
        } else {
            None
        }
    }

    pub fn remove_tag(&mut self, id: &ArticleID, tag: &Tag) -> Option<&Vec<Tag>> {
        if !self.contains(id) {
            return None;
        }

        if let Some(article_model) = self.models.iter_mut().find(|a| &a.id == id) {
            if let Some((index, _tag)) = article_model
                .tags
                .iter()
                .enumerate()
                .find(|(_i, t)| t.tag_id == tag.tag_id)
            {
                article_model.tags.remove(index);
            }
            Some(&article_model.tags)
        } else {
            None
        }
    }

    pub fn generate_diff<'a>(&'a self, new_list: &'a ArticleListModel) -> Edit<'_, Vec<ArticleListArticleModel>> {
        self.models.diff(&new_list.models)
    }

    fn sort(&mut self) {
        match self.sort {
            ArticleOrder::OldestFirst => {
                self.models.sort_by(|a, b| a.date.cmp(&b.date));
            }
            ArticleOrder::NewestFirst => {
                self.models.sort_by(|a, b| a.date.cmp(&b.date).reverse());
            }
        }
    }

    pub fn first(&self) -> Option<&ArticleListArticleModel> {
        self.models.first()
    }

    pub fn last(&self) -> Option<&ArticleListArticleModel> {
        self.models.iter().next_back()
    }

    pub fn get_article_model(&self, article_id: &ArticleID) -> Option<&ArticleListArticleModel> {
        self.models.iter().find(|model| &model.id == article_id)
    }

    pub fn get(&self, pos: usize) -> Option<&ArticleListArticleModel> {
        self.models.get(pos)
    }
}
