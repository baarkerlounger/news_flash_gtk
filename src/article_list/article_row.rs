use super::article_tags::ArticleRowTags;
use super::models::{ArticleGObject, GDateTime, GMarked, GRead, GTags, GThumbnail};
use crate::app::App;
use crate::i18n::i18n;
use crate::util::Util;
use chrono::Utc;
use futures::channel::oneshot;
use futures::future::FutureExt;
use gdk4::glib::ParamSpecBoxed;
use gdk4::Paintable;
use gio::{Menu, MenuItem};
use glib::{
    clone, subclass::*, ParamSpec, ParamSpecEnum, ParamSpecString, SignalHandlerId, StaticType, ToValue, Value,
};
use gtk4::{
    prelude::*, subclass::prelude::*, CompositeTemplate, ConstraintGuide, ConstraintLayout, GestureClick,
    GestureLongPress, Image, Inscription, Label, Picture, PopoverMenu, Revealer,
};
use log::warn;
use news_flash::models::{ArticleID, FavIcon, FeedID, Thumbnail};
use news_flash::util::text2html::Text2Html;
use once_cell::sync::Lazy;
use std::cell::{Cell, RefCell};
use std::ops::Deref;
use std::rc::Rc;

static SIGNALS: Lazy<Vec<Signal>> = Lazy::new(|| {
    vec![Signal::builder("activated")
        .param_types([ArticleRow::static_type()])
        .build()]
});

mod imp {
    use super::*;

    #[derive(Debug, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/article_list/row.blp")]
    pub struct ArticleRow {
        #[template_child]
        pub title_label: TemplateChild<Label>,
        #[template_child]
        pub summary_label: TemplateChild<Inscription>,
        #[template_child]
        pub feed_label: TemplateChild<Label>,
        #[template_child]
        pub date_label: TemplateChild<Label>,
        #[template_child]
        pub favicon: TemplateChild<Image>,
        #[template_child]
        pub thumb_image: TemplateChild<Picture>,
        #[template_child]
        pub thumb_revealer: TemplateChild<Revealer>,
        #[template_child]
        pub marked: TemplateChild<Image>,
        #[template_child]
        pub row_click: TemplateChild<GestureClick>,
        #[template_child]
        pub row_activate: TemplateChild<GestureClick>,
        #[template_child]
        pub row_long_press: TemplateChild<GestureLongPress>,
        #[template_child]
        pub article_tags: TemplateChild<ArticleRowTags>,

        pub article_id: RefCell<ArticleID>,
        pub feed_id: RefCell<FeedID>,
        pub feed_title: RefCell<String>,
        pub g_read: Cell<GRead>,
        pub g_marked: Cell<GMarked>,
        pub g_date: Cell<GDateTime>,
        pub g_tags: RefCell<GTags>,
        pub g_thumbnail: RefCell<GThumbnail>,

        pub row_hovered: RefCell<bool>,
        pub popover: Rc<RefCell<Option<PopoverMenu>>>,
        pub right_click_source: RefCell<Option<SignalHandlerId>>,
        pub long_press_source: RefCell<Option<SignalHandlerId>>,
    }

    impl Default for ArticleRow {
        fn default() -> Self {
            ArticleRow {
                title_label: TemplateChild::default(),
                summary_label: TemplateChild::default(),
                feed_label: TemplateChild::default(),
                date_label: TemplateChild::default(),
                favicon: TemplateChild::default(),
                thumb_image: TemplateChild::default(),
                thumb_revealer: TemplateChild::default(),
                marked: TemplateChild::default(),
                row_click: TemplateChild::default(),
                row_activate: TemplateChild::default(),
                row_long_press: TemplateChild::default(),
                article_tags: TemplateChild::default(),

                article_id: RefCell::new(ArticleID::new("")),
                feed_id: RefCell::new(FeedID::new("")),
                feed_title: RefCell::new("".into()),
                g_read: Cell::new(GRead::Unread),
                g_marked: Cell::new(GMarked::Unmarked),
                g_date: Cell::new(Utc::now().naive_utc().into()),
                g_tags: RefCell::new(Vec::new().into()),
                g_thumbnail: RefCell::new(None.into()),

                row_hovered: RefCell::new(false),
                popover: Rc::new(RefCell::new(None)),
                right_click_source: RefCell::new(None),
                long_press_source: RefCell::new(None),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ArticleRow {
        const NAME: &'static str = "ArticleRow";
        type ParentType = gtk4::Box;
        type Type = super::ArticleRow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ArticleRow {
        fn constructed(&self) {
            self.obj().init();
        }

        fn signals() -> &'static [Signal] {
            SIGNALS.as_ref()
        }

        fn properties() -> &'static [ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecEnum::builder::<GRead>("read").build(),
                    ParamSpecEnum::builder::<GMarked>("marked").build(),
                    ParamSpecString::builder("date").build(),
                    ParamSpecBoxed::builder::<GTags>("tags").build(),
                    ParamSpecBoxed::builder::<GThumbnail>("thumbnail").build(),
                    ParamSpecString::builder("feed-title").build(),
                ]
            });
            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &Value, pspec: &ParamSpec) {
            match pspec.name() {
                "read" => {
                    let input: GRead = value.get().expect("The value needs to be of type `GRead`.");
                    self.g_read.set(input);

                    self.obj().update_title_label_style();
                }
                "marked" => {
                    let input: GMarked = value.get().expect("The value needs to be of type `GMarked`.");
                    self.g_marked.replace(input);

                    self.obj().update_marked();
                }
                "date" => {
                    let input: String = value.get().expect("The value needs to be of type `string`.");
                    self.date_label.set_text(&input);
                }
                "tags" => {
                    let input = value.get().expect("The value needs to be of type `GTags`.");
                    self.g_tags.replace(input);

                    self.obj().update_tags();
                }
                "thumbnail" => {
                    let input: GThumbnail = value.get().expect("The value needs to be of type `GThumbnail`.");
                    let has_value = input.has_value();
                    self.g_thumbnail.replace(input);

                    if has_value {
                        self.obj().load_thumbnail();
                    } else {
                        self.thumb_revealer.set_reveal_child(false);
                    }
                }
                "feed-title" => {
                    let input: String = value.get().expect("The value needs to be of type `String`.");
                    self.feed_title.replace(input);

                    self.obj().update_feed_title();
                }
                _ => unreachable!(),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> Value {
            match pspec.name() {
                "read" => self.g_read.get().to_value(),
                "marked" => self.g_marked.get().to_value(),
                "date" => self.date_label.text().as_str().to_string().to_value(),
                "tags" => self.g_tags.borrow().to_value(),
                "thumbnail" => self.g_thumbnail.borrow().to_value(),
                "feed-title" => self.feed_title.borrow().to_value(),
                _ => unreachable!(),
            }
        }
    }

    impl WidgetImpl for ArticleRow {}

    impl BoxImpl for ArticleRow {}
}

glib::wrapper! {
    pub struct ArticleRow(ObjectSubclass<imp::ArticleRow>)
        @extends gtk4::Widget, gtk4::Box;
}

impl Default for ArticleRow {
    fn default() -> Self {
        glib::Object::new::<Self>()
    }
}

impl ArticleRow {
    pub fn new() -> Self {
        Self::default()
    }

    fn init(&self) {
        let imp = self.imp();
        imp.marked.set_from_icon_name(Some("starred-symbolic"));

        let guide = ConstraintGuide::builder().max_width(64).build();
        let layout_manager = ConstraintLayout::new();
        layout_manager.add_guide(guide);
        imp.thumb_image.set_layout_manager(Some(layout_manager));

        imp.row_activate.connect_released(clone!(
            @weak self as this => @default-panic, move |_gesture, times, _x, _y|
        {
            if times != 1 {
                return
            }

            this.activate();
        }));
    }

    pub fn bind_model(&self, article: &ArticleGObject) {
        let imp = self.imp();
        let is_same_article = *imp.article_id.borrow() == article.article_id();

        if article.read() != imp.g_read.get() {
            imp.g_read.set(article.read());
            self.update_title_label_style();
        }

        if article.marked() != imp.g_marked.get() {
            imp.g_marked.set(article.marked());
            self.update_marked();
        }

        if article.tags() != *imp.g_tags.borrow() {
            imp.g_tags.replace(article.tags());
            self.update_tags();
        }

        if article.feed_title() != *imp.feed_title.borrow() {
            imp.feed_title.replace(article.feed_title());
            self.update_feed_title();
        }

        if article.date() != imp.g_date.get() {
            imp.g_date.set(article.date());
            imp.date_label.set_text(&article.date_string());

            let title = &*article.title();
            if Text2Html::is_html(title) {
                imp.title_label.set_markup(&html2pango::markup(title));
            } else {
                imp.title_label.set_text(title);
            };
            imp.title_label.set_tooltip_text(Some(title));
        }

        let empty_paintable: Option<&Paintable> = None;

        if is_same_article && article.thumbnail() != *imp.g_thumbnail.borrow() {
            imp.g_thumbnail.replace(article.thumbnail());

            // clear thumbnail
            imp.thumb_image.set_paintable(empty_paintable);
            imp.thumb_image.set_size_request(-1, -1);

            if article.thumbnail().has_value() {
                self.load_thumbnail();
            } else {
                imp.thumb_revealer.set_reveal_child(false);
            }
        }

        if !is_same_article {
            imp.article_id.replace(article.article_id());
            imp.feed_id.replace(article.feed_id());

            imp.summary_label.set_text(Some(article.summary().as_str()));

            // clear favicon
            imp.favicon.set_from_icon_name(Some("application-rss+xml-symbolic"));
            // clear thumbnail
            imp.thumb_image.set_paintable(empty_paintable);
            imp.thumb_image.set_size_request(-1, -1);
            imp.thumb_revealer.set_reveal_child(false);

            if imp.right_click_source.borrow().is_none() {
                self.setup_right_click();
            }

            self.load_favicon();
            self.load_thumbnail();
        }
    }

    fn setup_right_click(&self) {
        let imp = self.imp();

        imp.right_click_source
            .replace(Some(imp.row_click.connect_released(clone!(
                @weak self as this => @default-panic, move |_gesture, times, _x, _y|
            {
                if times != 1 {
                    return
                }

                if App::default().content_page_state().borrow().get_offline() {
                    return
                }

                let imp = this.imp();
                if imp.popover.borrow().is_none() {
                    imp.popover.replace(Some(this.setup_context_popover()));
                }

                if let Some(popover) = imp.popover.borrow().as_ref() {
                    popover.popup();
                };
            }))));

        imp.long_press_source
            .replace(Some(imp.row_long_press.connect_pressed(clone!(
                @weak self as this => @default-panic, move |_gesture, _x, _y|
            {
                if App::default().content_page_state().borrow().get_offline() {
                    return
                }

                let imp = this.imp();
                if imp.popover.borrow().is_none() {
                    imp.popover.replace(Some(this.setup_context_popover()));
                }

                if let Some(popover) = imp.popover.borrow().as_ref() {
                    popover.popup();
                };
            }))));
    }

    fn setup_context_popover(&self) -> PopoverMenu {
        let imp = self.imp();
        let article_id = imp.article_id.borrow().clone();

        let model = Menu::new();
        let read_item = MenuItem::new(Some(&i18n("Toggle Read")), None);
        let star_item = MenuItem::new(Some(&i18n("Toggle Star")), None);
        let open_item = MenuItem::new(Some(&i18n("Open in Browser")), None);

        read_item.set_action_and_target_value(Some("win.toggle-article-read"), Some(&article_id.as_str().to_variant()));
        star_item.set_action_and_target_value(
            Some("win.toggle-article-marked"),
            Some(&article_id.as_str().to_variant()),
        );
        open_item.set_action_and_target_value(
            Some("win.open-article-in-browser"),
            Some(&article_id.as_str().to_variant()),
        );

        model.append_item(&read_item);
        model.append_item(&star_item);
        model.append_item(&open_item);

        let popover = PopoverMenu::from_model(Some(&model));
        popover.set_parent(self);
        popover
    }

    fn feed_id(&self) -> FeedID {
        self.imp().feed_id.borrow().clone()
    }

    fn load_favicon(&self) {
        let (oneshot_sender, receiver) = oneshot::channel::<Option<FavIcon>>();
        App::default().load_favicon(self.feed_id(), oneshot_sender);
        let this = self.clone();
        let glib_future = receiver.map(move |res| match res {
            Ok(Some(icon)) => {
                if let Some(data) = icon.data {
                    let bytes = glib::Bytes::from_owned(data);
                    let res = gdk4::Texture::from_bytes(&bytes);
                    if let Ok(texture) = res {
                        this.imp().favicon.set_from_paintable(Some(&texture));
                    } else if let Err(error) = res {
                        log::debug!("Favicon '{}': {}", icon.feed_id, error);
                    }
                }
            }
            Ok(None) => {
                warn!("Favicon does not contain image data.");
            }
            Err(_) => warn!("Receiving favicon failed."),
        });
        Util::glib_spawn_future(glib_future);
    }

    fn load_thumbnail(&self) {
        if App::default().settings().borrow().get_article_list_show_thumbs() {
            let (oneshot_sender, receiver) = oneshot::channel::<Option<Thumbnail>>();
            App::default().load_thumbnail(&self.id(), oneshot_sender);
            let this = self.clone();
            let glib_future = receiver.map(move |res| match res {
                Ok(Some(thumb)) => {
                    if let Some(data) = thumb.data {
                        let bytes = glib::Bytes::from_owned(data);
                        let texture = gdk4::Texture::from_bytes(&bytes);
                        if let Ok(texture) = texture {
                            let imp = this.imp();
                            imp.thumb_image.set_paintable(Some(&texture));
                            imp.thumb_image.set_size_request(64, 64);
                            imp.thumb_revealer.set_reveal_child(true);
                        }
                    }
                }
                Ok(None) => {
                    log::debug!("Thumbnail does not contain image data.");
                }
                Err(_) => warn!("Receiving favicon failed."),
            });
            Util::glib_spawn_future(glib_future);
        }
    }

    fn update_title_label_style(&self) {
        let imp = self.imp();
        match imp.g_read.get() {
            GRead::Read => imp.title_label.remove_css_class("heading"),
            GRead::Unread => imp.title_label.add_css_class("heading"),
        }
    }

    fn update_marked(&self) {
        let imp = self.imp();
        imp.marked.set_visible(imp.g_marked.get() == GMarked::Marked);
    }

    fn update_tags(&self) {
        let imp = self.imp();
        imp.article_tags.update_tags(imp.g_tags.borrow().deref().into());
    }

    fn update_feed_title(&self) {
        let imp = self.imp();
        imp.feed_label.set_text(&imp.feed_title.borrow());
    }

    pub fn id(&self) -> ArticleID {
        self.imp().article_id.borrow().clone()
    }

    pub fn read(&self) -> GRead {
        let imp = self.imp();
        imp.g_read.get()
    }

    fn activate(&self) {
        self.emit_by_name::<()>("activated", &[&self.clone()])
    }
}
