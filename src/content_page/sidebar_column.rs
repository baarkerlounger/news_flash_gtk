use crate::account_popover::AccountPopover;
use crate::app::App;
use crate::i18n::i18n;
use crate::sidebar::SideBar;
use crate::util::Util;
use eyre::{eyre, Result};
use gio::Menu;
use glib::clone;
use gtk4::{prelude::*, subclass::prelude::*, CompositeTemplate, Image, MenuButton, ToggleButton};
use libadwaita::HeaderBar;
use news_flash::models::{PluginCapabilities, PluginID, PluginIcon};
use news_flash::NewsFlash;
use std::cell::RefCell;

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/sidebar/column.blp")]
    pub struct SidebarColumn {
        #[template_child]
        pub headerbar: TemplateChild<HeaderBar>,
        #[template_child]
        pub account_button: TemplateChild<ToggleButton>,
        #[template_child]
        pub account_button_image: TemplateChild<Image>,
        #[template_child]
        pub add_button: TemplateChild<MenuButton>,
        #[template_child]
        pub menu_button: TemplateChild<MenuButton>,
        #[template_child]
        pub sidebar: TemplateChild<SideBar>,

        pub account_popover: RefCell<Option<AccountPopover>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SidebarColumn {
        const NAME: &'static str = "SidebarColumn";
        type ParentType = gtk4::Box;
        type Type = super::SidebarColumn;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for SidebarColumn {
        fn constructed(&self) {
            self.obj().init();
        }
    }

    impl WidgetImpl for SidebarColumn {}

    impl BoxImpl for SidebarColumn {}
}

glib::wrapper! {
    pub struct SidebarColumn(ObjectSubclass<imp::SidebarColumn>)
        @extends gtk4::Widget, gtk4::Box;
}

impl Default for SidebarColumn {
    fn default() -> Self {
        glib::Object::new::<Self>()
    }
}

impl SidebarColumn {
    pub fn new() -> Self {
        Self::default()
    }

    fn init(&self) {
        let imp = self.imp();

        let account_popover = AccountPopover::new(&*imp.account_button);
        let account_button = imp.account_button.get();
        account_popover.widget.connect_closed(move |_popover| {
            account_button.set_active(false);
        });
        imp.account_button.connect_clicked(
            clone!(@weak account_popover.widget as popover => @default-panic, move |toggle_button| {
                if toggle_button.is_active() {
                    popover.popup();
                } else {
                    popover.popdown();
                }
            }),
        );
        imp.account_popover.replace(Some(account_popover));

        self.setup_add_button();
        self.setup_menu_button();
    }

    fn account_popover(&self) -> AccountPopover {
        let imp = self.imp();
        imp.account_popover
            .borrow()
            .clone()
            .expect("SidebarColumn not initialized")
    }

    pub fn sidebar(&self) -> &SideBar {
        let imp = self.imp();
        &imp.sidebar
    }

    pub fn headerbar(&self) -> &HeaderBar {
        let imp = self.imp();
        &imp.headerbar
    }

    fn setup_menu_button(&self) {
        let imp = self.imp();

        let about_model = Menu::new();
        about_model.append(Some(&i18n("Preferences")), Some("win.settings"));
        about_model.append(Some(&i18n("Keyboard Shortcuts")), Some("win.shortcut-window"));
        about_model.append(Some(&i18n("About NewsFlash")), Some("win.about-window"));
        about_model.append(Some(&i18n("Quit")), Some("win.quit-application"));

        let im_export_model = Menu::new();
        im_export_model.append(Some(&i18n("Import OPML")), Some("win.import-opml"));
        im_export_model.append(Some(&i18n("Export OPML")), Some("win.export-opml"));

        let main_model = Menu::new();
        main_model.append(Some(&i18n("Discover Feeds")), Some("win.discover"));
        main_model.append_section(None, &im_export_model);
        main_model.append_section(None, &about_model);

        imp.menu_button.set_menu_model(Some(&main_model));
    }

    fn setup_add_button(&self) {
        let imp = self.imp();

        let menu_model = Menu::new();
        menu_model.append(Some(&i18n("Feed")), Some("win.add-feed"));
        menu_model.append(Some(&i18n("Category")), Some("win.add-category"));
        menu_model.append(Some(&i18n("Tag")), Some("win.add-tag"));

        imp.add_button.set_menu_model(Some(&menu_model));
    }

    pub fn set_account(&self, id: &PluginID, user_name: Option<&str>) -> Result<()> {
        let imp = self.imp();
        imp.account_button_image
            .set_from_icon_name(Some("feed-service-generic"));
        let user;

        let list = NewsFlash::list_backends();
        if let Some(info) = list.get(id) {
            user = match user_name {
                Some(user_name) => user_name.into(),
                None => info.name.clone(),
            };
            if let Some(plugin_icon) = &info.icon_symbolic {
                match plugin_icon {
                    PluginIcon::Vector(vector_icon) => {
                        let is_dark = libadwaita::StyleManager::default().is_dark();
                        let color = if is_dark { "#ffffff" } else { "#202020" };
                        let colored_data = Util::symbolic_icon_set_color(&vector_icon.data, color)?;
                        let bytes = glib::Bytes::from_owned(colored_data);
                        let texture = gdk4::Texture::from_bytes(&bytes);
                        imp.account_button_image.set_from_paintable(texture.ok().as_ref());
                    }
                    PluginIcon::Pixel(_icon) => {
                        log::warn!("Pixel based icon not valid for account button");
                    }
                }
            }
            if let Some(plugin_icon) = info.icon.clone() {
                match plugin_icon {
                    PluginIcon::Vector(vector_icon) => {
                        self.account_popover().set_account(Some(vector_icon), &user);
                    }
                    PluginIcon::Pixel(_icon) => {
                        log::warn!("Pixel based icon not valid for account widget");
                    }
                }
            }
        } else {
            let msg = format!("Try loading branding failed. Backend '{}' not found.", id);
            log::warn!("{}", msg);
            return Err(eyre!(msg));
        }

        Ok(())
    }

    pub fn update_features(&self) {
        let imp = self.imp();
        imp.add_button.set_sensitive(
            !App::default().content_page_state().borrow().get_offline()
                && App::default().features().contains(PluginCapabilities::ADD_REMOVE_FEEDS),
        );
    }

    pub fn set_offline(&self, offline: bool) {
        let imp = self.imp();

        imp.add_button
            .set_sensitive(!offline && App::default().features().contains(PluginCapabilities::ADD_REMOVE_FEEDS));
    }
}
