use super::item_row_dnd::ItemRowDnD;
use crate::app::App;
use crate::i18n::i18n;
use crate::sidebar::feed_list::models::{FeedListItemGObject, FeedListItemID, GFeedListItemID};
use crate::util::Util;
use futures::channel::oneshot;
use futures::future::FutureExt;
use gdk4::{ContentProvider, DragAction};
use gio::{Menu, MenuItem};
use glib::{clone, subclass, subclass::*, ParamSpec, ParamSpecString, ParamSpecUInt, SignalHandlerId, Value};
use gtk4::{
    prelude::*, subclass::prelude::*, Box, CompositeTemplate, DragSource, DropTarget, GestureClick, GestureLongPress,
    Image, Label, ListView, PopoverMenu, PositionType, TreeExpander, Widget,
};
use gtk4::{DragIcon, TreeListRow};
use log::warn;
use news_flash::models::{CategoryID, CategoryMapping, FavIcon, FeedMapping, NEWSFLASH_TOPLEVEL};
use once_cell::sync::Lazy;
use std::cell::{Cell, RefCell};
use std::rc::Rc;
use std::str;

static SIGNALS: Lazy<Vec<Signal>> = Lazy::new(|| {
    vec![Signal::builder("activated")
        .param_types([ItemRow::static_type()])
        .build()]
});

mod imp {
    use super::*;

    #[derive(Debug, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/sidebar/feedlist_item.blp")]
    pub struct ItemRow {
        #[template_child]
        pub expander: TemplateChild<TreeExpander>,
        #[template_child]
        pub row_click: TemplateChild<GestureClick>,
        #[template_child]
        pub row_activate: TemplateChild<GestureClick>,
        #[template_child]
        pub row_long_press: TemplateChild<GestureLongPress>,
        #[template_child]
        pub item_count_label: TemplateChild<Label>,
        #[template_child]
        pub title: TemplateChild<Label>,
        #[template_child]
        pub favicon: TemplateChild<Image>,
        #[template_child]
        pub drag_source: TemplateChild<DragSource>,
        #[template_child]
        pub drop_target: TemplateChild<DropTarget>,

        pub popover: Rc<RefCell<Option<PopoverMenu>>>,
        pub right_click_source: RefCell<Option<SignalHandlerId>>,
        pub long_press_source: RefCell<Option<SignalHandlerId>>,
        pub id: RefCell<FeedListItemID>,
        pub parent_id: RefCell<CategoryID>,
        pub label: RefCell<Rc<String>>,
        pub item_count: Cell<u32>,
        pub sort_index: Cell<i32>,
        pub list_item: Rc<RefCell<Option<TreeListRow>>>,
    }

    impl Default for ItemRow {
        fn default() -> Self {
            Self {
                expander: TemplateChild::default(),
                row_click: TemplateChild::default(),
                row_activate: TemplateChild::default(),
                row_long_press: TemplateChild::default(),
                item_count_label: TemplateChild::default(),
                title: TemplateChild::default(),
                favicon: TemplateChild::default(),
                drag_source: TemplateChild::default(),
                drop_target: TemplateChild::default(),

                popover: Rc::new(RefCell::new(None)),
                right_click_source: RefCell::new(None),
                long_press_source: RefCell::new(None),
                id: RefCell::new(FeedListItemID::Category(CategoryID::new(""))),
                parent_id: RefCell::new(CategoryID::new("")),
                label: RefCell::new(Rc::new(String::default())),
                item_count: Cell::new(0),
                sort_index: Cell::new(0),
                list_item: Rc::new(RefCell::new(None)),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ItemRow {
        const NAME: &'static str = "ItemRow";
        type ParentType = Box;
        type Type = super::ItemRow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ItemRow {
        fn signals() -> &'static [Signal] {
            SIGNALS.as_ref()
        }

        fn properties() -> &'static [ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecUInt::builder("item-count").build(),
                    ParamSpecString::builder("label").build(),
                ]
            });
            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &Value, pspec: &ParamSpec) {
            match pspec.name() {
                "item-count" => {
                    let input = value.get().expect("The value needs to be of type `u32`.");
                    self.item_count.set(input);
                    self.obj().update_item_count(input);
                }
                "label" => {
                    let input: String = value.get().expect("The value needs to be of type `string`.");
                    self.obj().update_title(&input);
                    self.label.replace(Rc::new(input));
                }
                _ => unreachable!(),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> Value {
            match pspec.name() {
                "item-count" => self.item_count.get().to_value(),
                "label" => (**self.label.borrow()).to_value(),
                _ => unreachable!(),
            }
        }

        fn constructed(&self) {
            self.obj().init();
        }
    }

    impl WidgetImpl for ItemRow {}

    impl BoxImpl for ItemRow {}
}

glib::wrapper! {
    pub struct ItemRow(ObjectSubclass<imp::ItemRow>)
        @extends Widget, Box;
}

impl Default for ItemRow {
    fn default() -> Self {
        glib::Object::new::<Self>()
    }
}

impl ItemRow {
    pub fn new() -> Self {
        Self::default()
    }

    fn init(&self) {
        self.setup_drop_target();

        self.imp().row_activate.connect_released(clone!(
            @weak self as this => @default-panic, move |_gesture, times, _x, _y|
        {
            if times != 1 {
                return
            }

            this.activate();
        }));
    }

    fn clear_dnd_highlight(&self) {
        if let Some(prev) = self
            .parent()
            .and_then(|p| p.prev_sibling())
            .and_then(|c| c.first_child())
        {
            prev.remove_css_class("dnd-bottom");
        }
        if let Some(next) = self
            .parent()
            .and_then(|p| p.next_sibling())
            .and_then(|c| c.first_child())
        {
            next.remove_css_class("dnd-bottom");
        }
        self.remove_css_class("dnd-top");
        self.remove_css_class("dnd-bottom");
    }

    fn hightlight_dnd(&self, y: f64) {
        if y < self.height() as f64 / 2.0 {
            if let Some(prev) = self
                .parent()
                .and_then(|p| p.prev_sibling())
                .and_then(|c| c.first_child())
            {
                prev.add_css_class("dnd-bottom");
            } else {
                self.add_css_class("dnd-top");
            }
        } else {
            self.add_css_class("dnd-bottom");
        }
    }

    fn calc_drop_destination(&self, y: f64) -> (CategoryID, i32) {
        let item_id = self.id();
        let sort_index = self.sort_index();
        log::debug!("drop onto {item_id:?} index {sort_index}");
        if y < self.height() as f64 / 2.0 {
            if let Some(prev) = self
                .parent()
                .and_then(|p| p.prev_sibling())
                .and_then(|c| c.first_child())
                .and_downcast::<ItemRow>()
            {
                if item_id.is_category() {
                    (NEWSFLASH_TOPLEVEL.clone(), prev.sort_index() + 1)
                } else if let FeedListItemID::Category(category) = prev.id() {
                    (category, 0)
                } else {
                    (prev.parent_id(), prev.sort_index() + 1)
                }
            } else {
                // no prev row -> we at the very top of the list
                (NEWSFLASH_TOPLEVEL.clone(), 0)
            }
        } else if let FeedListItemID::Category(category) = item_id {
            if self.is_expanded() {
                (category, 0)
            } else {
                (NEWSFLASH_TOPLEVEL.clone(), self.sort_index() + 1)
            }
        } else {
            (self.parent_id(), self.sort_index() + 1)
        }
    }

    fn start_category_drag(&self) {
        if let Some(feed_list) = self.parent().and_then(|p| p.parent()).and_downcast::<ListView>() {
            let mut list_item = feed_list.first_child();
            while let Some(item) = list_item.clone() {
                if let Some(item_row) = item.first_child().and_downcast::<ItemRow>() {
                    item_row.collapse();
                    item_row.unset_expander();
                }

                list_item = item.next_sibling();
            }
        }
    }

    fn collapse(&self) {
        if let Some(list_row) = self.imp().expander.list_row() {
            list_row.set_expanded(false);
        }
    }

    fn is_expanded(&self) -> bool {
        self.imp()
            .expander
            .list_row()
            .map(|row| row.is_expanded())
            .unwrap_or(false)
    }

    fn unset_expander(&self) {
        self.imp().expander.set_list_row(None);
    }

    fn finish_category_drag(&self) {
        if let Some(feed_list) = self.parent().and_then(|p| p.parent()).and_downcast::<ListView>() {
            let mut list_item = feed_list.first_child();
            while let Some(item) = list_item.clone() {
                if let Some(item_row) = item.first_child().and_downcast::<ItemRow>() {
                    item_row.reset_expander();
                }

                list_item = item.next_sibling();
            }
        }
    }

    fn reset_expander(&self) {
        let imp = self.imp();
        imp.expander.set_list_row(imp.list_item.borrow().as_ref());
    }

    pub fn bind_model(&self, model: &FeedListItemGObject, list_row: &TreeListRow) {
        let imp = self.imp();
        let is_same_item = *imp.id.borrow() == model.id();

        let features = App::default().features();
        let support_mutation = features.support_mutation();

        imp.id.replace(model.id());
        imp.parent_id.replace(model.parent_id());
        imp.label.replace(model.label());
        imp.item_count.set(model.item_count());
        imp.sort_index.set(model.sort_index());

        imp.expander.set_list_row(Some(list_row));
        imp.list_item.replace(Some(list_row.clone()));

        if imp.right_click_source.borrow().is_none() {
            self.setup_right_click();
        }

        if support_mutation {
            self.setup_drag_source();
        }

        if !is_same_item {
            self.update_item_count(model.item_count());
            self.update_title(model.label().as_str());
            self.update_favicon(Some(model.id()));
        }
    }

    fn setup_drop_target(&self) {
        let imp = self.imp();
        imp.drop_target.set_types(&[GFeedListItemID::static_type()]);
        imp.drop_target.set_actions(DragAction::MOVE);
        imp.drop_target.connect_enter(
            clone!(@weak self as this => @default-panic, move |_drop_target, _x, y| {
                this.clear_dnd_highlight();
                this.hightlight_dnd(y);
                DragAction::MOVE
            }),
        );
        imp.drop_target.connect_motion(
            clone!(@weak self as this => @default-panic, move |_drop_target, _x, y| {
                this.clear_dnd_highlight();
                this.hightlight_dnd(y);
                DragAction::MOVE
            }),
        );
        imp.drop_target
            .connect_leave(clone!(@weak self as this => @default-panic, move |_drop_target| {
                this.clear_dnd_highlight();
            }));
        imp.drop_target.connect_drop(
            clone!(@weak self as this => @default-panic, move |_drop_target, value, _x, y| {
                this.clear_dnd_highlight();
                let (parent_id, sort_index) = this.calc_drop_destination(y);
                let item_id: GFeedListItemID = value.get::<GFeedListItemID>().unwrap();
                let item_id: FeedListItemID = item_id.into();

                if let FeedListItemID::Category(category_id) = item_id {
                    let new_mapping = CategoryMapping { parent_id, category_id, sort_index: Some(sort_index) };
                    log::debug!("move category {new_mapping:?}");
                    App::default().move_category(new_mapping);
                } else if let FeedListItemID::Feed(current_mapping) = item_id {
                    let  new_mapping = FeedMapping { feed_id: current_mapping.feed_id.clone(), category_id: parent_id, sort_index: Some(sort_index) };
                    log::debug!("move feed from {current_mapping:?} to {new_mapping:?}");
                    App::default().move_feed((*current_mapping).clone(), new_mapping);
                }
                true
            }),
        );
    }

    fn setup_drag_source(&self) {
        let imp = self.imp();
        let id: GFeedListItemID = imp.id.borrow().clone().into();
        let content_provider = ContentProvider::for_value(&id.to_value());
        imp.drag_source.set_content(Some(&content_provider));
        imp.drag_source.set_actions(DragAction::MOVE);

        imp.drag_source
            .connect_drag_begin(clone!(@weak self as this => @default-panic,move |_source, drag| {
                this.set_opacity(0.5);
                let imp = this.imp();

                if imp.id.borrow().is_category() {
                    this.start_category_drag();
                }

                let drag_icon = DragIcon::for_drag(drag).downcast::<DragIcon>().unwrap();
                let widget = ItemRowDnD::new(imp.id.borrow().clone(), imp.label.borrow().as_str(), imp.item_count.get());
                widget.set_width_request(this.parent().map(|p| p.width() - 16).unwrap_or(100));
                widget.set_height_request(this.height() - 2);
                drag_icon.set_child(Some(&widget));
            }));

        imp.drag_source.connect_drag_cancel(
            clone!(@weak self as this => @default-panic,move |_source, _drag, _cancel_reason| {
                this.set_opacity(1.0);
                if this.imp().id.borrow().is_category() {
                    this.finish_category_drag();
                }
                true
            }),
        );

        imp.drag_source.connect_drag_end(
            clone!(@weak self as this => @default-panic,move |_source, _drag, _delete_data| {
                this.set_opacity(1.0);
                if this.imp().id.borrow().is_category() {
                    this.finish_category_drag();
                }
            }),
        );
    }

    fn setup_right_click(&self) {
        let imp = self.imp();

        imp.right_click_source
            .replace(Some(imp.row_click.connect_released(clone!(
                @weak self as this => @default-panic, move |_gesture, times, _x, _y|
            {
                if times != 1 {
                    return
                }

                if App::default().content_page_state().borrow().get_offline() {
                    return
                }

                let imp = this.imp();
                if imp.popover.borrow().is_none() {
                    imp.popover.replace(Some(this.setup_context_popover()));
                }

                if let Some(popover) = imp.popover.borrow().as_ref() {
                    popover.popup();
                };
            }))));

        imp.long_press_source
            .replace(Some(imp.row_long_press.connect_pressed(clone!(
                @weak self as this => @default-panic, move |_gesture, _x, _y|
            {
                if App::default().content_page_state().borrow().get_offline() {
                    return
                }

                let imp = this.imp();
                if imp.popover.borrow().is_none() {
                    imp.popover.replace(Some(this.setup_context_popover()));
                }

                if let Some(popover) = imp.popover.borrow().as_ref() {
                    popover.popup();
                };
            }))));
    }

    fn setup_context_popover(&self) -> PopoverMenu {
        let imp = self.imp();

        let model = Menu::new();
        let edit_item = MenuItem::new(Some(&i18n("Edit")), None);
        let delete_item = MenuItem::new(Some(&i18n("Delete")), None);

        match &*imp.id.borrow() {
            FeedListItemID::Feed(feed_mapping) => {
                let tuple_variant = (
                    feed_mapping.feed_id.as_str().to_string(),
                    feed_mapping.category_id.as_str().to_string(),
                )
                    .to_variant();
                edit_item.set_action_and_target_value(Some("win.edit-feed-dialog"), Some(&tuple_variant));

                let tuple_variant = (
                    feed_mapping.feed_id.as_str().to_string(),
                    imp.label.borrow().to_string(),
                )
                    .to_variant();
                delete_item.set_action_and_target_value(Some("win.enqueue-delete-feed"), Some(&tuple_variant));
            }
            FeedListItemID::Category(category_id) => {
                let variant = category_id.as_str().to_variant();
                edit_item.set_action_and_target_value(Some("win.edit-category-dialog"), Some(&variant));

                let tuple_variant = (category_id.as_str().to_string(), imp.label.borrow().to_string()).to_variant();
                delete_item.set_action_and_target_value(Some("win.enqueue-delete-category"), Some(&tuple_variant));
            }
        }

        model.append_item(&edit_item);
        model.append_item(&delete_item);

        let popover = PopoverMenu::from_model(Some(&model));
        popover.set_parent(self);
        popover.set_position(PositionType::Bottom);
        popover
    }

    fn update_item_count(&self, count: u32) {
        let imp = self.imp();

        if count > 0 {
            imp.item_count_label.set_label(&count.to_string());
            imp.item_count_label.set_visible(true);
        } else {
            imp.item_count_label.set_visible(false);
        }
    }

    fn update_favicon(&self, feed_id: Option<FeedListItemID>) {
        let imp = self.imp();

        if let Some(FeedListItemID::Feed(feed_mapping)) = feed_id {
            let (sender, receiver) = oneshot::channel::<Option<FavIcon>>();
            App::default().load_favicon(feed_mapping.feed_id.clone(), sender);

            let this = self.clone();
            let glib_future = receiver.map(move |res| match res {
                Ok(Some(icon)) => {
                    if let Some(data) = icon.data {
                        let bytes = glib::Bytes::from_owned(data);
                        let texture = gdk4::Texture::from_bytes(&bytes);
                        if let Ok(texture) = texture {
                            this.imp().favicon.set_from_paintable(Some(&texture));
                        } else if let Err(error) = texture {
                            log::debug!("Favicon '{}': {}", icon.feed_id, error);
                        }
                    }
                }
                Ok(None) => {
                    warn!("Favicon does not contain image data.");
                }
                Err(_) => warn!("Receiving favicon failed."),
            });

            Util::glib_spawn_future(glib_future);
        } else {
            imp.favicon.set_visible(false);
        }
    }

    fn update_title(&self, title: &str) {
        let imp = self.imp();
        imp.title.set_label(title);
    }

    fn activate(&self) {
        self.emit_by_name::<()>("activated", &[&self.clone()]);
    }

    fn id(&self) -> FeedListItemID {
        self.imp().id.borrow().clone()
    }

    fn parent_id(&self) -> CategoryID {
        self.imp().parent_id.borrow().clone()
    }

    fn sort_index(&self) -> i32 {
        self.imp().sort_index.get()
    }
}
