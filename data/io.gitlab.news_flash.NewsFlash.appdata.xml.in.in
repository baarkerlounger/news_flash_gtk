<?xml version="1.0" encoding="UTF-8"?>
<!-- Bilal Elmoussaoui 2019 <bilal.elmoussaoui@gnome.org> -->
<component type="desktop-application">
  <id>@appid@</id>
  <metadata_license>CC0</metadata_license>
  <project_license>GPL-3.0+</project_license>
  <name>NewsFlash</name>
  <summary>Keep up with your feeds</summary>
  <translation type="gettext">@pkgname@</translation>
  <content_rating type="oars-1.0" />
  <description>
    <p>
      NewsFlash is a program designed to complement an already existing web-based RSS reader account.
    </p>

    <p>
      It combines all the advantages of web based services like syncing across all your devices with everything you expect
      from a modern desktop program: Desktop notifications, fast search and filtering, tagging, handy keyboard shortcuts
      and having access to all your articles for as long as you like.
    </p>
  </description>
  <screenshots>
    <screenshot type="default">
      <image>https://gitlab.com/news_flash/news_flash_gtk/-/raw/master/data/screenshots/Main.png</image>
    </screenshot>
    <screenshot>
      <image>https://gitlab.com/news_flash/news_flash_gtk/-/raw/master/data/screenshots/Dark.png</image>
    </screenshot>
    <screenshot>
      <image>https://gitlab.com/news_flash/news_flash_gtk/-/raw/master/data/screenshots/Preferences.png</image>
    </screenshot>
    <screenshot>
      <image>https://gitlab.com/news_flash/news_flash_gtk/-/raw/master/data/screenshots/Adaptive.png</image>
    </screenshot>
  </screenshots>
  <url type="homepage">https://gitlab.com/news_flash/news_flash_gtk</url>
  <url type="vcs-browser">https://gitlab.com/news_flash/news_flash_gtk</url>
  <url type="bugtracker">https://gitlab.com/news_flash/news_flash_gtk/-/issues</url>
  <url type="translate">https://hosted.weblate.org/projects/newsflash/news_flash_gtk/</url>
  <supports>
    <control>keyboard</control>
    <control>pointing</control>
    <control>touch</control>
  </supports>
  <requires>
    <display_length compare="ge">360</display_length>
  </requires>
  <custom>
    <value key="GnomeSoftware::key-colors">[(255, 194, 77), (0, 0, 0)]</value>
  </custom>
  <releases>
    <release version="3.0.0" timestamp="1695314133">
      <description>
        <ul>
          <li>New look and lots of small refinements for the UI</li>
          <li>Drag and drop support for feeds and categories</li>
          <li>New edit dialogs for feeds, categories and tags</li>
          <li>Allow to move feeds between categories via edit dialog</li>
          <li>Custom mathjax inline delimiter adjustable per feed</li>
          <li>Allow sorting feeds and categories alphabetically</li>
          <li>Fix symbolic icon</li>
          <li>Fix sidebar iteration with "only relevant" enabled</li>
          <li>Fix switch state in settings dialog</li>
          <li>Improve error messages in case of API limit</li>
          <li>Local RSS: improve favicon source selection</li>
          <li>Favicons: avoid unnecessary parallel downloads</li>
          <li>Speedup: use tokio runtime instead of threadpool</li>
          <li>Speedup: (re)create HTTP client only when the network changed</li>
          <li>Offline Mode: detect automatically when offline or online</li>
          <li>Offline Mode: allow updating read/starred status of articles</li>
          <li>Article View: make title unclickable if article doesn't provide an URL</li>
          <li>Option to allow NewsFlash to autostart</li>
          <li>Article List: section by day</li>
          <li>Article List: Update layout</li>
          <li>Article List: automatic updates to reflect more tag and feed changes</li>
          <li>Image dialog</li>
          <li>Video dialog</li>
          <li>Fix embeded youtube videos</li>
          <li>Use adwaita SplitViews for resizing sidebar and article list</li>
          <li>Replace Add popover with dialog</li>
          <li>Extract relevant image from HTML as thumbnail</li>
          <li>Save/Restore the most important properties of the UI</li>
        </ul>
      </description>
    </release>
    <release version="2.3.1" timestamp="1691139280">
      <description>
        <p>Update the App-Id to be able to verify it on flathub</p>
      </description>
    </release>
    <release version="2.3.0" timestamp="1683132746">
      <description>
        <ul>
          <li>Massive update to the internal article scraper</li>
          <li>Remove Mercury Parser</li>
          <li>Use Background Apps Portal</li>
          <li>Update to stable Gtk 4 WebKit API</li>
          <li>Article View: adjust dark background color to match adwaita</li>
          <li>Local RSS: remove scripts in content</li>
          <li>Nextcloud News: fix creating API Url</li>
        </ul>
      </description>
    </release>
    <release version="2.2.4" timestamp="1670910276">
      <description>
        <ul>
          <li>Add up/down arrow shortcuts to article list</li>
          <li>Fever: fix sync regression</li>
          <li>Fix article list slide transitions</li>
        </ul>
      </description>
    </release>
    <release version="2.2.3" timestamp="1670567327">
      <description>
        <ul>
          <li>Fever: fix crash when receiving invalid auth data</li>
          <li>Nextcloud News: handle potential negative timestamps</li>
          <li>Fix crash when changing article theme</li>
          <li>Add shortcut to copy article URL to clipboard</li>
          <li>Better loading of articles (fixes potential duplicate articles in list)</li>
          <li>Updated error handling</li>
          <li>Allow uncategorized feeds</li>
        </ul>
      </description>
    </release>
    <release version="2.2.2" timestamp="1668927221">
      <description>
        <ul>
          <li>Miniflux: fix handling token and username</li>
        </ul>
      </description>
    </release>
    <release version="2.2.1" timestamp="1668923779">
      <description>
        <ul>
          <li>Fix crash with sync on startup enabled</li>
          <li>Store Webkit ContentFilter in proper directory</li>
          <li>Inoreader: fix updating api secret after login</li>
        </ul>
      </description>
    </release>
    <release version="2.2.0" timestamp="1668873437">
      <description>
        <ul>
          <li>Inoreader: support custom API secrets</li>
          <li>GReader: better error messages</li>
          <li>Miniflux: support login via token</li>
          <li>Article View: block loading of external style sheets</li>
          <li>Article View: fix stretched images</li>
          <li>Add Popover: empty category dropdown after account reset</li>
        </ul>
      </description>
    </release>
    <release version="2.1.4" timestamp="1667978989">
      <description>
        <p>Hotfix Release to fix crash</p>
      </description>
    </release>
    <release version="2.1.3" timestamp="1667978989">
      <description>
        <p>Features:</p>
        <ul>
          <li>GReader: support enclosures</li>
        </ul>
        <p>Bugfixes:</p>
        <ul>
          <li>GReader: mark feeds/categories as read</li>
          <li>GReader: fix rename category</li>
          <li>Uncategorized feeds not showing up in exported OPML-files</li>
          <li>Only show Discover Dialog after login with Local RSS</li>
        </ul>
      </description>
    </release>
    <release version="2.1.2" timestamp="1664709603">
      <description>
        <p>Bugfixes:</p>
        <ul>
          <li>fix commandline parser</li>
        </ul>
      </description>
    </release>
    <release version="2.1.1" timestamp="1664703669">
      <description>
        <p>Bugfixes:</p>
        <ul>
          <li>UI: fix scrolling articles on touchscreen</li>
          <li>UI: fix fullscreen video</li>
          <li>fix crash when scraping article content</li>
          <li>FreshRSS: fix login issue</li>
        </ul>
      </description>
    </release>
    <release version="2.1.0" timestamp="1664457038">
      <description>
        <p>Features:</p>
        <ul>
          <li>UI: display tags in article list</li>
          <li>UI: share articles</li>
          <li>UI: update responsive layout</li>
        </ul>
        <p>Bugfixes:</p>
        <ul>
          <li>Local RSS: no content showing for certain feeds (e.g. TWIM)</li>
          <li>FreshRSS: autocomplete URL to API endpoint</li>
          <li>FreshRSS: use special characters as part of password</li>
          <li>FreshRSS: can't rename tag</li>
          <li>Miniflux &amp; Fever: can't star articles</li>
          <li>UI: can't switch between provided and scraped content</li>
          <li>UI: clicking enclosure doesn't work</li>
        </ul>
      </description>
    </release>
    <release version="2.0.1" timestamp="1663949098">
      <description>
        <p>Bugfixes:</p>
        <ul>
          <li>database: fix a migration issue</li>
          <li>fix new categories not showing up in the sidebar</li>
        </ul>
      </description>
    </release>
    <release version="2.0.0" timestamp="1663928146">
      <description>
        <p>This release marks the biggest update for NewsFlash so far.</p>
        <p>Most importantly the application was ported to Gtk4 &amp; libadwaita. This came along with lots of internal code improvements.</p>
        <p>Features:</p>
        <ul>
          <li>Refined Interface</li>
          <li>Imporived Phone Layout</li>
          <li>Fresh RSS Support</li>
          <li>Nextcloud News Support</li>
          <li>Reworked Tagging</li>
          <li>Custom Sync Interval</li>
          <li>Sync on Startup</li>
          <li>Hide Articles dated in the Future</li>
          <li>Fullscreen Videos</li>
          <li>Article View: Custom Content Width</li>
          <li>Article View: Custom Line Height</li>
          <li>Article View: Syntax highlighting</li>
          <li>Better Flatpak support via Portals</li>
          <li>Translations</li>
          <li>Keep Articles without Feed</li>
          <li>Option to not sync on metered connections</li>
          <li>Advanced option to disable image auto loading</li>
        </ul>
      </description>
    </release>
    <release version="1.5.1" timestamp="1636136007">
      <description>
        <p>Bugfixes:</p>
        <ul>
          <li>feedbin: fix sync error</li>
          <li>feedbin: fix thumbnails</li>
          <li>feedbin: fix enclosures</li>
          <li>small article visual fix</li>
        </ul>
      </description>
    </release>
    <release version="1.5.0" timestamp="1630734105">
      <description>
        <p>Features:</p>
        <ul>
          <li>Support for Inoreader</li>
          <li>feedly no longer supported due to expired API key</li>
        </ul>
        <p>Bugfixes/Improvements:</p>
        <ul>
          <li>NewsBlur: access denied on login</li>
          <li>Mercury Parser: failed due to invalid string format</li>
          <li>Startup parameters being ignored</li>
          <li>Article View: add inspect menu entry</li>
        </ul>
      </description>
    </release>
    <release version="1.4.3" timestamp="1630734105">
      <description>
        <p>Bugfixes/Improvements:</p>
        <ul>
          <li>Discover: fix crash</li>
          <li>Sidebar: Improve selection and iteration</li>
        </ul>
      </description>
    </release>
    <release version="1.4.2" timestamp="1624183726">
      <description>
        <p>Bugfixes/Improvements:</p>
        <ul>
          <li>Switch out remote Mercury parser for local one</li>
          <li>Filter out socks4 proxies not supported by http library</li>
          <li>Complete URLs in content with xml:base attribute</li>
          <li>Local RSS: Use feed URL as ID if the feed doesn't provide one</li>
        </ul>
      </description>
    </release>
    <release version="1.4.1" timestamp="1620878448">
      <description>
        <p>Bugfixes/Improvements:</p>
        <ul>
          <li>Use OPML crate for ex/import</li>
          <li>Indent OPML xml on export</li>
          <li>Improve feedly sync behaviour</li>
          <li>Local RSS: Handle relative URLs of enclosures</li>
          <li>Enclosures now opening from withing flatpak sandbox</li>
        </ul>
      </description>
    </release>
    <release version="1.4.0" timestamp="1615712440">
      <description>
        <p>This release brings support for thumbnails in the article list and enclosures for articles.</p>
      </description>
    </release>
    <release version="1.3.0" timestamp="1614450097">
      <description>
        <p>NewsFlash is now able to download and display thumbnails if the service or feed provides them.</p>
        <p>Bugfixes/Improvements:</p>
        <ul>
          <li>Feed-List: Feeds showing even parent category was collapsed</li>
          <li>Feed-List: Relevant feed filter not working</li>
          <li>Feed-List: Drag and Drop not working</li>
        </ul>
      </description>
    </release>
    <release version="1.2.3" timestamp="1613323388">
      <description>
        <p>Hotfix miniflux sync behaviour</p>
      </description>
    </release>
    <release version="1.2.2" timestamp="1613244649">
      <description>
        <p>Bugfixes/Improvements:</p>
        <ul>
          <li>Fix foreign key constraint upgrading database scheme</li>
          <li>Miniflux: better sync</li>
          <li>Arrow-key navigation in sidebar</li>
          <li>Improve article-list updates</li>
          <li>Fix "keep articles" 1 week setting</li>
          <li>Clarify the "keep articles" setting</li>
        </ul>
      </description>
    </release>
    <release version="1.2.1" timestamp="1612290968">
      <description>
        <p>Hotfix for broken Database migration</p>
        <p>Bugfixes:</p>
        <ul>
          <li>Database migration: copy old article data</li>
          <li>local RSS: avoid reassigning current date when no date information is available</li>
          <li>GUI: fix offline/online popover positioning</li>
          <li>GUI: re-enable arrow navigation in article-list</li>
        </ul>
      </description>
    </release>
    <release version="1.2.0" timestamp="1611481498">
      <description>
        <p>Features:</p>
        <ul>
          <li>Support for NewsBlur</li>
          <li>feedly: port to new collections API</li>
          <li>Option to limit duration articles are kept in the database</li>
          <li>Option to hide feeds without unread/starred articles</li>
          <li>Update article with new content like forum posts</li>
          <li>local RSS: support for MediaRSS</li>
        </ul>
        <p>Bugfixes:</p>
        <ul>
          <li>Fix regression in database that lead to foreign key constraints failing</li>
          <li>feedly: fix token not refreshing</li>
          <li>Remove unnecessary menu entry</li>
          <li>Various updated libraries and small code improvements</li>
        </ul>
      </description>
    </release>
    <release version="1.1.1" timestamp="1607618209">
      <p>Hotfix: unable to launch NewsFlash via desktop-file</p>
    </release>
    <release version="1.1.0" timestamp="1607545566">
      <description>
        <p>Features:</p>
        <ul>
          <li>migrate to libhandy1</li>
          <li>GUI can now be squeezed to 360px horizontally to better fit mobile</li>
        </ul>
        <p>Bugfixes:</p>
        <ul>
          <li>local RSS: don't fail on invalid xml attributes</li>
          <li>local RSS: unique feed ID for feeds without useful data</li>
          <li>miniflux: parsing article date when timezone is set to UTC</li>
          <li>fever: sync with articles without feed</li>
          <li>feedly: sync with missing stream title</li>
          <li>feedly: sync with duplicate tags field of entry</li>
          <li>unable to re-add feed after deletion</li>
        </ul>
      </description>
    </release>
    <release version="1.0.5" timestamp="1597407063">
      <description>
        <p>Bugfixes:</p>
        <ul>
          <li>local RSS: enconding support</li>
          <li>local RSS: support content:encoded for RSS 1.0</li>
          <li>database: issue when deleting feed-mappings</li>
          <li>feedly: ignore negative published timestamps</li>
        </ul>
      </description>
    </release>
    <release version="1.0.4" timestamp="1596965575">
      <description>
        <p>Bugfixes:</p>
        <ul>
          <li>feedly: don't require fingerprint</li>
          <li>feedly: don't require origin title</li>
          <li>enable gzip feature of reqwest client</li>
          <li>local RSS: smarter selection of article link</li>
          <li>keybinding: fix next-item</li>
        </ul>
      </description>
    </release>
    <release version="1.0.3" timestamp="1595870068">
      <description>
        <p>Bugfixes:</p>
        <ul>
          <li>web login page invisible</li>
          <li>OPML import containing feeds without category</li>
        </ul>
      </description>
    </release>
    <release version="1.0.2" timestamp="1594908289">
      <description>
        <p>Bugfixes:</p>
        <ul>
          <li>Article List: Load more articles even when scrolling very fast</li>
          <li>feedbin: don't require entries to have a summary</li>
          <li>feedbin: reset cache after a sync failed</li>
          <li>feedbin: ignore articles without feed</li>
          <li>fever: fix issue with tt-rss fever plugin</li>
          <li>Import atom feeds from OPML exported with liferea</li>
        </ul>
      </description>
    </release>
    <release version="1.0.1" timestamp="1594699843">
      <description>
        <p>Bugfixes:</p>
        <ul>
          <li>Close about dialog with no headerbar</li>
          <li>Don't trigger shortcuts twice when article view is in focus</li>
          <li>Firefox issues when opening URL with it</li>
        </ul>
      </description>
    </release>
    <release version="1.0.0" timestamp="1590389867">
      <description>
        <p>Bugfixes:</p>
        <ul>
          <li>Updated App Icon</li>
          <li>Set webview background color according to Gtk theme</li>
          <li>Allow flatpak to access 'Downloads' to save images from the article view</li>
          <li>Remove article view content justification</li>
          <li>React to dark theme preference set from outside</li>
          <li>Add "busy timeout" for database connections</li>
          <li>Fever: escape HTML entities in title</li>
          <li>feedly: ignore articles without existing feed</li>
          <li>feedly: make stream_id optional</li>
          <li>OPML import: use `No Title` if both `title` and `text` are missing</li>
          <li>OPML import: always parse feed if no website is available</li>
          <li>OPML import: Spin the update button to indicate long processing</li>
          <li>Article list: don't insert row when already contained in model</li>
          <li>Toggle article state based of article list selection instead of article view</li>
          <li>Convert timestamp into local timezone for display</li>
          <li>Fix 32 bit build</li>
          <li>Fix copying text by not inhibiting all key presses in webview</li>
          <li>Fix appicon not properly shown: don't call show_all() in application constructor</li>
          <li>Update date string in article list if a new day has begun</li>
          <li>Correctly show initial state of article theme setting</li>
          <li>Crash when opening and using settings a second time</li>
          <li>Unreadable links in Gruvbox theme</li>
        </ul>
      </description>
    </release>
    <release version="0.9.9" timestamp="1590130667">
      <description>
        <p>Features:</p>
        <ul>
          <li>Feedbin support</li>
          <li>Mercury Parser hosted by Feedbin as fallback for the internal scraper</li>
          <li>Readability as a fallback for the internal scraper</li>
          <li>Support for uncategorized feeds and "generated/fake" categories</li>
          <li>Save images to Downloads folder</li>
          <li>Switch between scraped and original content</li>
          <li>Search full text of scraped article</li>
        </ul>

        <p>Bugfixes:</p>
        <ul>
          <li>Fever: accepting both string and integer for 'last_refreshed_on_time'</li>
          <li>Fever: crash when syncing uncategorized feeds</li>
          <li>Fever: can't login with HTTP basic auth</li>
          <li>Fever: slow bulk updating of articles</li>
          <li>Local: feeds from the same website could overwrite each other</li>
          <li>Local: Improve layout when only summary is present</li>
          <li>Local: better handle misformed dates in ATOM feeds</li>
          <li>First sync downloads too many articles</li>
          <li>Can't apply tag to article after creating it</li>
          <li>Better error handling for non-web logins</li>
          <li>UI elements visible that shouldn't</li>
          <li>Problems opening links from articles</li>
          <li>Next article shortcut skipping articles</li>
          <li>Debug files written to disk</li>
          <li>Improved right-to-left language support</li>
          <li>Prefer scraped content when exporting an article</li>
        </ul>
      </description>
    </release>
    <release version="0.9.8" timestamp="1588229867">
      <description>
				First public release
			</description>
    </release>
  </releases>
  <translation type="gettext">newsflash</translation>
  <launchable type="desktop-id">@appid@.desktop</launchable>
  <developer_name>Jan Lukas Gernert</developer_name>
</component>